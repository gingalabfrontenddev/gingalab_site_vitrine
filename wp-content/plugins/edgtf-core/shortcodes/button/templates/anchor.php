<a itemprop="url" href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>" <?php orkan_edge_inline_style($button_styles); ?> <?php orkan_edge_class_attribute($button_classes); ?> <?php echo orkan_edge_get_inline_attrs($button_data); ?> <?php echo orkan_edge_get_inline_attrs($button_custom_attrs); ?>>
    <span class="edgtf-btn-text"><?php echo esc_html($text); ?></span>
    <?php echo orkan_edge_icon_collections()->renderIcon($icon, $icon_pack); ?>
    <?php if ($hover_animation === 'yes') {?>
        <span class="edgtf-btn-hover-item" <?php orkan_edge_inline_style($button_hover_styles); ?>></span>
    <?php } ?>
</a>