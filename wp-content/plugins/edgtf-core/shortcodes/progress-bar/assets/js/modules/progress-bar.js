(function($) {
	'use strict';
	
	var progressBar = {};
	edgtf.modules.progressBar = progressBar;
	
	progressBar.edgtfInitProgressBars = edgtfInitProgressBars;
	
	
	progressBar.edgtfOnDocumentReady = edgtfOnDocumentReady;
	
	$(document).ready(edgtfOnDocumentReady);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function edgtfOnDocumentReady() {
		edgtfInitProgressBars();
	}
	
	/*
	 **	Horizontal progress bars shortcode
	 */
	function edgtfInitProgressBars() {
		var progressBar = $('.edgtf-progress-bar');
		
		if (progressBar.length) {
			progressBar.each(function (i) {
				var thisBar = $(this),
					thisBarContent = thisBar.find('.edgtf-pb-content'),
					thisBarPercent = thisBar.find('.edgtf-pb-percent'),
					percentage = thisBarContent.data('percentage');

				var animateBar = function (thisBar, thisBarContent, percentage, i) {
					setTimeout(function() {
						edgtfInitToCounterProgressBar(thisBar, percentage);
						thisBarContent.stop().animate({'width': percentage + '%'}, 1200);
					}, i*150);
				}
				
				if (!edgtf.htmlEl.hasClass('touch')) {
					if (thisBar.closest('.edgtf-vertical-split-slider').length) {
						$(document).one('edgtfProgressBarTrigger', function() {
							animateBar(thisBar, thisBarContent, percentage, i);
						});
					} else {
						thisBar.appear(function() {
							animateBar(thisBar, thisBarContent, percentage, i);
						});
					}
				} else {
					thisBarContent.css('width', percentage + '%');
					thisBarPercent.css('opacity', '1');
					thisBarPercent.text(percentage);
				}
			});
		}
	}
	
	/*
	 **	Counter for horizontal progress bars percent from zero to defined percent
	 */
	function edgtfInitToCounterProgressBar(progressBar, $percentage){
		var percentage = parseFloat($percentage),
			percent = progressBar.find('.edgtf-pb-percent');
		
		if(percent.length) {
			percent.each(function() {
				var thisPercent = $(this);
				thisPercent.css('opacity', '1');
				
				thisPercent.countTo({
					from: 0,
					to: percentage,
					speed: 1200,
					refreshInterval: 50
				});
			});
		}
	}
	
})(jQuery);