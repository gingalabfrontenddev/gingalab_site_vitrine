(function($) {
    'use strict';

    var dynamicIntroSection = {};
    edgtf.modules.dynamicIntroSection = dynamicIntroSection;

    dynamicIntroSection.edgtfInitDynamicIntroSection = edgtfInitDynamicIntroSection;
    dynamicIntroSection.edgtfOnDocumentReady = edgtfOnDocumentReady;

    $(document).ready(edgtfOnDocumentReady);

    /*
     All functions to be called on $(document).ready() should be in this function
     */
    function edgtfOnDocumentReady() {
        edgtfInitDynamicIntroSection();
    }

    /**
     * Init Dynamic Intro Section Shortcode
     */
    function edgtfInitDynamicIntroSection() {
        var intro = $('#edgtf-dynamic-intro-section'),
            sectionHeight;

        if (intro.length) {
            //check for layout and behavior classes
            var singleScrollBehavior = intro.hasClass('edgtf-dis-single-scroll') ? true : false,
                scrollTrigger = false,
                withContent = intro.hasClass('edgtf-dis-with-text') ? true : false,
                withDot = intro.hasClass('edgtf-dis-with-dot') ? true : false;

            //fullscreen calcs
            var fullscreenCalcs = function() {
                sectionHeight = edgtf.windowHeight - intro.offset().top;

                if (edgtf.body.hasClass('edgtf-paspartu-enabled')) {
                    var passepartoutSize = parseInt($('.edgtf-wrapper').css('padding-top'));

                    sectionHeight -= passepartoutSize;
                }

                intro.css('height', sectionHeight);
            }

            //perform single scroll fx
            var singleScrollFx = function() {
                if (singleScrollBehavior) {
                    var sectionOffset = intro.offset().top,
                        shortcodeArea = sectionHeight + sectionOffset,
                        scrollingForward = false,
                        pageJump = false,
                        normalScroll = true;

                    $(window).scrollTop(sectionOffset);

                    var edgtfWheelHandler = function() {
                        if  ($(window).scrollTop() < shortcodeArea) {
                            normalScroll = false;
                        }

                        var edgtfScrollTo = function () {
                            pageJump = true;
                            $('html, body').animate({
                                scrollTop: shortcodeArea
                            }, 1000, 'easeInOutQuint', function() {
                                pageJump = false;
                                normalScroll = true;
                            });
                        }

                        //update values
                        $(window).scroll(function(){
                            if (edgtf.scroll < shortcodeArea && scrollingForward) {
                                normalScroll = false;
                            }
                        });

                        window.addEventListener('wheel', function(event) {
                            if (edgtf.scroll  < shortcodeArea) {
                                var wheelScroll = event.deltaY;

                                if (wheelScroll > 0) {
                                    scrollingForward = true;
                                } else {
                                    scrollingForward = false;
                                }

                                if (!pageJump && !normalScroll) {
                                    if (scrollingForward) {
                                        event.preventDefault();
                                        edgtfScrollTo();
                                    }
                                } else {
                                    if (!normalScroll) {
                                        event.preventDefault();
                                    }
                                    if (normalScroll && !scrollingForward) {
                                        pageJump = false;
                                        normalScroll = false;
                                        event.preventDefault();
                                    }   
                                }
                            }

                            if (pageJump) {
                                event.preventDefault();
                            }
                        });

                        //scrollbar click
                        $(document).on('mousedown', function(event){
                            if(($(window).outerWidth() <= event.pageX) && (edgtf.scroll <= shortcodeArea)) {
                                event.preventDefault();
                                edgtfScrollTo();
                            }
                        });
                    }

                    //prevent mousewheel scroll
                    window.addEventListener('wheel', function (event) {
                        if (!scrollTrigger) {
                            event.preventDefault();
                            $(window).scrollTop(sectionOffset);
                        }
                    });

                    //prevent scrollbar scroll
                    window.addEventListener('scroll', function () {
                        if (!scrollTrigger) {
                            $(window).scrollTop(sectionOffset);
                        }
                    })  

                    $(document).one('edgtfDisWheelReady', function() {
                        edgtfWheelHandler();
                    });
                }
            }

            //perform scroll down on click
            var clickScrollFx = function() {
                if (singleScrollBehavior) {
                    var scrollIcon  = intro.find('.edgtf-dis-icon-holder'),
                        sectionOffset,
                        shortcodeArea;

                    scrollIcon.on('click', function(e){
                        if (scrollTrigger) {
                            e.preventDefault();
                            sectionOffset = intro.offset().top,
                            shortcodeArea = sectionHeight + sectionOffset;
                            $('html, body').animate({
                                scrollTop: shortcodeArea
                            }, 1000, 'easeInOutQuint', function() {
                                return false;
                            });
                        }
                    });
                }
            }

            //perform auto scroll fx
            var autoScrollFx = function() {
                if (!singleScrollBehavior) {
                    var sectionOffset = intro.offset().top;

                    $(window).scrollTop(sectionOffset);

                    //prevent scroll
                    window.addEventListener('scroll', function () {
                        if (!scrollTrigger) {
                            $(window).scrollTop(sectionOffset);
                        }
                    });

                    $(document).on('edgtfDisAutoScroll', function(){
                        $('html, body').animate({
                            scrollTop:  sectionOffset + sectionHeight,
                        }, 1000, 'easeInOutQuint', function() {
                            if (!withContent) {
                                $(window).scrollTop(0);
                                intro.fadeOut(0);
                                return false;
                            }
                        });
                    });
                }
            }

            //fade in loading message
            var fadeInLoadingMessage = function() {
                setTimeout(function(){
                    $('#edgtf-dis-loading-message > span').fadeIn(200);
                }, 500);
            }

            //remove loading message 
            var removeLoadingMessage = function() {
                $('#edgtf-dis-loading-message').fadeOut(300, function(){
                    $(this).remove();
                    animateImages();
                });
            }

            //animate images
            var animateImages = function() {
                var imagesHolder = intro.find('.edgtf-dis-images-holder').addClass('edgtf-dis-uncover'),
                    cols = intro.find('.edgtf-dis-col'),
                    duration = 5000;

                cols.each(function(i){
                    var col = $(this),
                        yDiff = Math.round(sectionHeight - col.position().top),
                        offset = i % 2 ? - yDiff : yDiff,
                        d = (Math.random() * 0.3).toFixed(2) + 1,
                        startY = offset*0.9,
                        endY = -offset * d;

                    col.css({'transform' : 'translate3d(0px, '+startY*d*2+'px, 0px)'});

                    $.keyframe.define([{
                        name: 'col-'+i,
                        '0%':   {'transform': 'translate3d(0px, '+startY+'px, 0px)'},
                        '80%':   {'transform': 'translate3d(0px, '+endY*1+'px, 0px)'},
                        '100%':   {'transform': 'translate3d(0px, '+endY*3.5+'px, 0px)'}
                    }]);

                    col.addClass('edgtf-dis-animate').playKeyframe({
                        name: 'col-'+i, 
                        duration: duration+'ms',
                        timingFunction: 'cubic-bezier(0, 0.73, 1, 0.29)',
                        complete: function(){
                            if (i == cols.length - 1) {
                                imagesHolder.addClass('edgtf-dis-hide');
                            }
                        }
                    });

                    imagesHolder.delay(0.5*duration).animate({opacity:0 }, 600, 'easeInOutQuint', function() {
                        $(document).trigger('edgtfDisImagesDone');
                    });
                });
            }

            //animate text
            var animateText = function() {
                if (withContent) {
                    var contentHolder = intro.find('.edgtf-dis-content-holder').css('visibility', 'visible'),
                        titleHolder = intro.find('.edgtf-dis-title-holder').css('visibility', 'visible'),
                        title = intro.find('.edgtf-dis-title'),
                        titleMask = intro.find('.edgtf-dis-title-mask'),
                        descriptionHolder = intro.find('.edgtf-dis-description-holder');


                    titleHolder.addClass('edgtf-animate-mask');
                    titleMask.one(edgtf.transitionEnd, function() {
                        title.css('visibility', 'visible');
                        descriptionHolder.addClass('edgtf-show-description');
                        intro.find('.edgtf-dot').addClass('edgtf-show-dot');

                        titleMask.one(edgtf.transitionEnd, function() {
                            $(document).trigger('edgtfTextDone');
                        });
                    });
                } else {
                    $(document).trigger('edgtfTextDone');
                }
            }

            //animate icon
            var animateIcon = function() {
                var iconHolder = intro.find('.edgtf-dis-icon-holder').addClass('edgtf-show-icon'),
                    scrollArrow = intro.find('.edgtf-dis-arrow');

                iconHolder.one(edgtf.transitionEnd, function() {
                    scrollArrow.addClass('edgtf-scroll-bounce');
                    scrollTrigger = true;
                    $(document).trigger('edgtfDisWheelReady');
                }); 
            }

            $(document).one('edgtfDisImagesDone', function(){
                animateText();
            });

            $(document).one('edgtfTextDone', function(){
                if (singleScrollBehavior) {
                    animateIcon();
                } else {
                    $(document).trigger('edgtfDisAutoScroll');
                    scrollTrigger = true;
                }
            });            


            intro.waitForImages(function(){
                fullscreenCalcs();
                fadeInLoadingMessage();
                singleScrollFx();
                clickScrollFx();
                autoScrollFx();
            });

            $(window).load(function(){
                removeLoadingMessage();
            })

            $(window).resize(function(){
                fullscreenCalcs();
            });
        }
    }


})(jQuery);
