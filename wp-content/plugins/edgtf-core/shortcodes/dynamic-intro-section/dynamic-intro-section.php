<?php
namespace EdgeCore\CPT\Shortcodes\DynamicIntroSection;

use EdgeCore\Lib;

class DynamicIntroSection implements Lib\ShortcodeInterface {
	private $base;
	
	public function __construct() {
		$this->base = 'edgtf_dynamic_intro_section';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
	public function vcMap() {
		if(function_exists('vc_map')) {
			vc_map(
				array(
					'name'                      => esc_html__( 'Edge Dynamic Intro Section', 'edge-cpt' ),
					'base'                      => $this->getBase(),
					'category'                  => esc_html__( 'by EDGE', 'edge-cpt' ),
					'icon'                      => 'icon-wpb-dynamic-intro-section extended-custom-icon',
                    'allowed_container_element' => 'vc_row',
					'params'                    => array(
						array(
							'type'        => 'textfield',
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'edgtf-core' ),
							'admin_label' => true
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'description',
							'heading'     => esc_html__( 'Description', 'edgtf-core' ),
							'dependency'  => array( 'element' => 'title', 'not_empty' => true ),
							'admin_label' => true
						),
						array(
						    'type'        => 'dropdown',
						    'param_name'  => 'dot_after_title',
						    'heading'  => esc_html__( 'Add Dot After Title', 'edge-cpt' ),
                            'value'       => array_flip( orkan_edge_get_yes_no_select_array( false, true ) ),
							'dependency'  => array( 'element' => 'title', 'not_empty' => true ),
						),
						array(
						    'type'        => 'dropdown',
						    'param_name'  => 'behavior',
						    'heading'  => esc_html__( 'Behavior', 'edge-cpt' ),
					    	'value'      => array(
					    		esc_html__( 'Single Scroll To Content', 'edge-cpt' )	=> 'single-scroll',
					    		esc_html__( 'Auto Scroll To Content', 'edge-cpt' )    => 'auto-scroll',
						    ),
							'dependency'  => array( 'element' => 'title', 'not_empty' => true ),
						    'save_always' => true
						),
                        array(
                            'type' => 'param_group',
                            'heading' => esc_html__( 'First Column Images', 'edge-cpt' ),
                            'param_name' => 'first_column_images',
                            'params' => array(
                            	array(
                            	    'type'        => 'attach_image',
                            	    'param_name'  => 'item_image',
                            	    'heading'     => esc_html__( 'Item Image', 'edge-cpt' ),
                            	),
                            ),
                            'group' => esc_html__( 'First Column', 'edge-cpt' ),
                        ),
                        array(
                            'type' => 'param_group',
                            'heading' => esc_html__( 'Second Column Images', 'edge-cpt' ),
                            'param_name' => 'second_column_images',
                            'params' => array(
                            	array(
                            	    'type'        => 'attach_image',
                            	    'param_name'  => 'item_image',
                            	    'heading'     => esc_html__( 'Item Image', 'edge-cpt' ),
                            	),
                            ),
                            'group' => esc_html__( 'Second Column', 'edge-cpt' ),
                        ),
                        array(
                            'type' => 'param_group',
                            'heading' => esc_html__( 'Third Column Images', 'edge-cpt' ),
                            'param_name' => 'third_column_images',
                            'params' => array(
                            	array(
                            	    'type'        => 'attach_image',
                            	    'param_name'  => 'item_image',
                            	    'heading'     => esc_html__( 'Item Image', 'edge-cpt' ),
                            	),
                            ),
                            'group' => esc_html__( 'Third Column', 'edge-cpt' ),
                        ),
                        array(
                            'type' => 'param_group',
                            'heading' => esc_html__( 'Fourth Column Images', 'edge-cpt' ),
                            'param_name' => 'fourth_column_images',
                            'params' => array(
                            	array(
                            	    'type'        => 'attach_image',
                            	    'param_name'  => 'item_image',
                            	    'heading'     => esc_html__( 'Item Image', 'edge-cpt' ),
                            	),
                            ),
                            'group' => esc_html__( 'Fourth Column', 'edge-cpt' ),
                        ),
                        array(
                            'type' => 'param_group',
                            'heading' => esc_html__( 'Fifth Column Images', 'edge-cpt' ),
                            'param_name' => 'fifth_column_images',
                            'params' => array(
                            	array(
                            	    'type'        => 'attach_image',
                            	    'param_name'  => 'item_image',
                            	    'heading'     => esc_html__( 'Item Image', 'edge-cpt' ),
                            	),
                            ),
                            'group' => esc_html__( 'Fifth Column', 'edge-cpt' ),
                        ),
                    )
				)
			);
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array(
            'title'             			=> '',
            'description'	    			=> '',
            'dot_after_title'	    		=> 'yes',
            'behavior'     					=> '',
            'first_column_images'           => '',
            'second_column_images'          => '',
            'third_column_images'           => '',
            'fourth_column_images'         	=> '',
            'fifth_column_images'          	=> '',
		);
		
		$params = shortcode_atts($args, $atts);
		$params['holder_classes'] = $this->getHolderClasses($params);
        $params['content'] = $content;
        $params['col_images_1'] = json_decode(urldecode($params['first_column_images']), true);
        $params['col_images_2'] = json_decode(urldecode($params['second_column_images']), true);
		$params['col_images_3'] = json_decode(urldecode($params['third_column_images']), true);
		$params['col_images_4'] = json_decode(urldecode($params['fourth_column_images']), true);
		$params['col_images_5'] = json_decode(urldecode($params['fifth_column_images']), true);

		$html = edgtf_core_get_shortcode_module_template_part('templates/dynamic-intro-section', 'dynamic-intro-section', '', $params);
		
		return $html;
	}


	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClasses($params){
		$holderClasses = array();

		if (!empty($params['title'])) {
			$holderClasses[] = 'edgtf-dis-with-text';

			if ($params['dot_after_title'] == 'yes') {
				$holderClasses[] = 'edgtf-dis-with-dot';
			}

			if (!empty($params['behavior'])) {
				$holderClasses[] = 'edgtf-dis-'. $params['behavior'];
			}
		}

		return implode(' ', $holderClasses);
	}
}