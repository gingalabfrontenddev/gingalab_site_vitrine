<div id="edgtf-dynamic-intro-section" class="<?php echo esc_attr($holder_classes); ?>">
	<?php if (!empty($title)) : ?>
		<div class="edgtf-dis-content-holder">
			<div class="edgtf-dis-text-holder">
				<div class="edgtf-dis-title-holder">
					<h1 class="edgtf-dis-title">
						<span class="edgtf-dis-title-text"><?php echo esc_attr($title); ?></span>
						<?php if ($dot_after_title == 'yes') { ?>
							<span class="edgtf-dot">.</span>
						<?php } ?>
					</h1>
					<span class="edgtf-dis-title-mask"></span>
				</div>
				<div class="edgtf-dis-description-holder">
					<p class="edgtf-dis-description"><?php echo esc_attr($description); ?></p>
				</div>
			</div>
			<?php if ($behavior == 'single-scroll') : ?>
				<div class="edgtf-dis-icon-holder">
					<span class="edgtf-dis-mouse">
						<span class="edgtf-dis-scroll"></span>
					</span>
					<span class="edgtf-dis-arrow">
						<i class="fa fa-chevron-down" aria-hidden="true"></i>
					</span>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<div class="edgtf-dis-images-holder">
		<?php 
			$i = 0;
			for ($i = 1; $i <= 5; $i++) : ?>
    	<div class="edgtf-dis-col edgtf-dis-col-<?php echo esc_attr($i); ?>">
            <?php foreach(${'col_images_'.$i} as $item) : ?>
                <?php if (!empty($item['item_image'])) : ?>
                    <div class="edgtf-dis-image-wrapper">
                        <img class="edgtf-dis-image" src="<?php echo wp_get_attachment_url($item['item_image']); ?>" alt="<?php echo get_the_title($item['item_image']); ?>" />
                    </div>
                <?php endif; ?>
			<?php endforeach; ?>
    	</div>
    	<?php endfor; ?>
	</div>
	<div id="edgtf-dis-loading-message">
		<span><?php echo esc_html__('Loading...', 'orkan'); ?></span>
	</div>
</div>