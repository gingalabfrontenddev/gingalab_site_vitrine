<?php if(orkan_edge_options()->getOptionValue('portfolio_single_hide_pagination') !== 'yes') {
	$nav_same_category = orkan_edge_options()->getOptionValue('portfolio_single_nav_same_category') == 'yes';
	?>
    <div class="edgtf-ps-navigation">
        <?php if(get_previous_post() !== '') : ?>
            <div class="edgtf-ps-prev">
	            <?php if ( $nav_same_category ) {
		            $prev_img = '';
		            
		            if ( get_adjacent_post( true, false, true, 'portfolio-category' ) !== "" ) {
			            $prev_post = get_adjacent_post( true, false, true, 'portfolio-category' );
			            $prev_img  = orkan_edge_generate_thumbnail( get_post_thumbnail_id( $prev_post->ID ), false, 130, 130 );
		            }
		            
		            previous_post_link( '%link', '<span class="edgtf-ps-nav-mark fa fa-chevron-left"></span>' . $prev_img, true, '', 'portfolio-category' );
	            } else {
		            $prev_post = get_previous_post();
		            $prev_img  = orkan_edge_generate_thumbnail( get_post_thumbnail_id( $prev_post->ID ), false, 130, 130 );
		            
		            previous_post_link( '%link', '<span class="edgtf-ps-nav-mark fa fa-chevron-left"></span>' . $prev_img );
	            } ?>
            </div>
        <?php endif; ?>

        <?php if(get_next_post() !== '') : ?>
            <div class="edgtf-ps-next">
	            <?php if ( $nav_same_category ) {
		            $next_img = '';
		            
		            if ( get_adjacent_post( true, false, false, 'portfolio-category' ) !== "" ) {
			            $next_post = get_adjacent_post( true, false, false, 'portfolio-category' );
			            $next_img  = orkan_edge_generate_thumbnail( get_post_thumbnail_id( $next_post->ID ), false, 130, 130 );
		            }
		            
		            next_post_link( '%link', $next_img . '<span class="edgtf-ps-nav-mark fa fa-chevron-right"></span>', true, '', 'portfolio-category' );
	            } else {
		            $next_post = get_next_post();
		            $next_img  = orkan_edge_generate_thumbnail( get_post_thumbnail_id( $next_post->ID ), false, 130, 130 );
		            
		            next_post_link( '%link', $next_img . '<span class="edgtf-ps-nav-mark fa fa-chevron-right"></span>' );
	            } ?>
            </div>
        <?php endif; ?>
    </div>
<?php } ?>