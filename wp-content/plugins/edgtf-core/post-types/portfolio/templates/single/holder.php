<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="edgtf-container">
	    <div class="edgtf-container-inner clearfix">
	        <div class="edgtf-portfolio-single-holder <?php echo esc_attr($holder_classes); ?>">
	            <?php if(post_password_required()) {
	                echo get_the_password_form();
	            } else {
	                do_action('orkan_edge_portfolio_page_before_content');
	            
	                edgtf_core_get_cpt_single_module_template_part('templates/single/layout-collections/'.$item_layout, 'portfolio', '', $params);
	            
	                do_action('orkan_edge_portfolio_page_after_content');
	
	                edgtf_core_get_cpt_single_module_template_part('templates/single/parts/related-posts', 'portfolio', $item_layout);
	                
	                edgtf_core_get_cpt_single_module_template_part('templates/single/parts/comments', 'portfolio');
	            } ?>
	        </div>
	    </div>
	</div>
	
	<?php edgtf_core_get_cpt_single_module_template_part('templates/single/parts/navigation', 'portfolio', $item_layout); ?>
<?php endwhile; endif; ?>