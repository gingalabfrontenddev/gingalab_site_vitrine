<?php

if ( ! function_exists( 'orkan_edge_portfolio_category_additional_fields' ) ) {
	function orkan_edge_portfolio_category_additional_fields() {
		
		$fields = orkan_edge_add_taxonomy_fields(
			array(
				'scope' => 'portfolio-category',
				'name'  => 'portfolio_category_options'
			)
		);
		
		orkan_edge_add_taxonomy_field(
			array(
				'name'   => 'edgtf_portfolio_category_image_meta',
				'type'   => 'image',
				'label'  => esc_html__( 'Category Image', 'edgt-core' ),
				'parent' => $fields
			)
		);
	}
	
	add_action( 'orkan_edge_custom_taxonomy_fields', 'orkan_edge_portfolio_category_additional_fields' );
}