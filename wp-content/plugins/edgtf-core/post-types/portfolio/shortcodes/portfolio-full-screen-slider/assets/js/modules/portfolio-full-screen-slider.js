(function($) {
    'use strict';

    var portfolioFullScreenSlider = {};
    edgtf.modules.edgtfInitPortfolioFullScreenSlider = edgtfInitPortfolioFullScreenSlider;

    portfolioFullScreenSlider.edgtfOnDocumentReady = edgtfOnDocumentReady;

    $(document).ready(edgtfOnDocumentReady);
    
    /* 
     All functions to be called on $(document).ready() should be in this function
     */
    function edgtfOnDocumentReady() {
	    edgtfInitPortfolioFullScreenSlider();
    }


    /**
     * Initializes portfolio full screen slider logic
     */
    function edgtfInitPortfolioFullScreenSlider(){
        var holders = $('.edgtf-portfolio-full-screen-slider-holder');

        if(holders.length){
	        holders.each(function(){
                var holder = $(this),
                	infoBlock = $('#edgtf-ptf-info-block'),
                	infoHolder = infoBlock.find('.edgtf-pli-info-holder'),
                	titleArea = infoBlock.find('.edgtf-pli-title'),
                	dateArea = infoBlock.find('.edgtf-pli-date'),
                	excerptInfoArea = infoBlock.find('.edgtf-pli-excerpt'),
                	categoryInfoArea = infoBlock.find('.edgtf-pli-category-info > p'),
                	dateInfoArea = infoBlock.find('.edgtf-pli-date-info > p'),
                	tagInfoArea = infoBlock.find('.edgtf-pli-tag-info > p'),
                	shareListInfoArea = infoBlock.find('.edgtf-social-share-holder > ul'),
                	infoOpener = infoBlock.find('.edgtf-pli-up-arrow'),
                	mousewheelScroll = holder.find('.edgtf-portfolio-list-holder').data('enable-mousewheel-scroll') == 'yes' ? true : false,
	                swiperInstance = holder.find('.swiper-container'),
            		swiperSlider = new Swiper (swiperInstance, {
            			loop: true,
            			direction: 'vertical',
            			slidesPerView: 1,
            			speed: 1000,
            			mousewheel: mousewheelScroll,
            			autoplay: {
            			    delay: 2800,
            			},
            			navigation: {
            			  nextEl: '.edgtf-next-icon',
            			  prevEl: '.edgtf-prev-icon',
            			},
            			init: false
		    		});

            	var setInfo = function() {
            		var activeSlide = swiperInstance.find('.swiper-slide-active'),
            			titleHtml = activeSlide.find('.edgtf-pli-title').html(),
            			dateVal = activeSlide.find('.edgtf-pli-date').text(),
            			excerptVal = activeSlide.find('.edgtf-pli-excerpt').text(),
            			categoryHtml = activeSlide.find('.edgtf-pli-category-info > p').html(),
            			tagHtml = activeSlide.find('.edgtf-pli-tag-info > p').html(),
            			shareListHtml = activeSlide.find('.edgtf-social-share-holder > ul').html();

            		titleArea.html(titleHtml);
            		dateArea.text(dateVal);
            		excerptInfoArea.text(excerptVal);
            		categoryInfoArea.html(categoryHtml);
            		dateInfoArea.text(dateVal);
            		tagInfoArea.html(tagHtml);
            		shareListInfoArea.html(shareListHtml);
            	}

            	var infoToggle = function() {
            		infoOpener.on('click', function(){
            			edgtf.body.toggleClass('edgtf-pfss-item-is-active');
        				infoBlock.toggleClass('edgtf-active');
        				infoOpener.toggleClass('edgtf-active');
        				dateArea.toggleClass('edgtf-hide');
        				infoHolder.toggleClass('edgtf-show');

        				if (edgtf.body.hasClass('edgtf-pfss-item-is-active')) {
        					swiperSlider.autoplay.stop();
        				} else {
        					swiperSlider.autoplay.start();
        					swiperSlider.slideNext();
        				}
            		});
            	}

		        var fullscreenCalcs = function() {
		        	var topOffset = holder.offset().top,
                    	passepartoutHeight = edgtf.body.hasClass('edgtf-paspartu-enabled') ? parseInt( $('.edgtf-wrapper').css('padding-top'), 10 ) : 0;

			        holder.css('height', edgtf.windowHeight - topOffset - passepartoutHeight);
		        }


            	swiperSlider.on('init', function(){
            		holder.addClass('edgtf-initialized');
			    });

            	swiperSlider.on('slideChangeTransitionStart', function(){
		        	setInfo();
			    });

		        holder.waitForImages(function(){
		        	fullscreenCalcs();
                	swiperSlider.init();
		        	infoToggle();
		        });

		        $(window).resize(function(){
		        	fullscreenCalcs();
		        });
		
		        // items.each(function(){
			       //  var item = $(this),
			       //      arrow = item.find('.edgtf-pli-up-arrow');
			
			       //  arrow.on('click', function (e) {
				      //   e.preventDefault();
				
				      //   if (arrow.hasClass('edgtf-active')) {
					     //    edgtf.body.removeClass('edgtf-pfss-item-is-active');
					     //    arrow.removeClass('edgtf-active');
					     //    item.removeClass('edgtf-pfss-active');
				      //   } else {
					     //    edgtf.body.addClass('edgtf-pfss-item-is-active');
					     //    arrow.addClass('edgtf-active');
					     //    item.addClass('edgtf-pfss-active');
				      //   }
			       //  });
		        // });
		
		        // $(document).on('owl_onchanged_slider_trigger', function(){
			       //  edgtf.body.removeClass('edgtf-pfss-item-is-active');
			       //  items.find('.edgtf-pli-up-arrow').removeClass('edgtf-active');
			       //  items.removeClass('edgtf-pfss-active');
		        // });
            });
        }
    }

})(jQuery);