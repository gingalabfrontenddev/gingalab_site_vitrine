<div class="edgtf-portfolio-list-holder <?php echo esc_attr($holder_classes); ?>" <?php echo wp_kses($holder_data, array('data')); ?>>
	<?php echo edgtf_core_get_cpt_shortcode_module_template_part('portfolio', 'portfolio-list', 'parts/filter', '', $params); ?>
	<div class="edgtf-pl-inner edgtf-outer-space <?php echo esc_attr($holder_inner_classes); ?> clearfix">
		<?php if ($item_style == 'full-screen') : ?>
			<div class="swiper-container"><div class="swiper-wrapper">
		<?php endif; ?>
		<?php
			if($query_results->have_posts()):
				while ( $query_results->have_posts() ) : $query_results->the_post();
					echo edgtf_core_get_cpt_shortcode_module_template_part('portfolio', 'portfolio-list', 'portfolio-item', $item_type, $params);
				endwhile;
			else:
				echo edgtf_core_get_cpt_shortcode_module_template_part('portfolio', 'portfolio-list', 'parts/posts-not-found');
			endif;
		
			wp_reset_postdata();
		?>
		<?php if ($item_style == 'full-screen') : ?>
			</div>
			<?php if ($enable_navigation == 'yes') : ?>
				<span class="edgtf-prev"><span class="edgtf-prev-icon fa fa-chevron-top"></span></span>
				<span class="edgtf-next"><span class="edgtf-next-icon fa fa-chevron-bottom"></span></span>
			<?php endif; ?>
			</div>
			<div id="edgtf-ptf-info-block">
				<div class="edgtf-pli-text-holder">
					<div class="edgtf-pli-text-wrapper">
						<div class="edgtf-pli-text">
							<div class="edgtf-pli-text-inner">
								<a class="edgtf-pli-up-arrow" href="#"><i class="fa fa fa-chevron-up"></i></a>
								<h2 itemprop="name" class="edgtf-pli-title entry-title"></h2>
								<div class="edgtf-pli-date"></div>
								<div class="edgtf-pli-info-holder">
									<p itemprop="description" class="edgtf-pli-excerpt"></p>
									<div class="edgtf-pli-category-info edgtf-pli-info">
										<h5 class="edgtf-pli-info-title">Category:</h5>
										<p><a itemprop="url" href="#"></a></p>
									</div>
									<div class="edgtf-pli-date-info edgtf-pli-info">
										<h5 class="edgtf-pli-info-title">Date:</h5>
										<p></p>
									</div>
									<div class="edgtf-pli-tag-info edgtf-pli-info">
										<h5 class="edgtf-pli-info-title">Tag:</h5>
										<p><a itemprop="url" href="#"></a></p>
									</div>
									<div class="edgtf-pli-share-info edgtf-pli-info">
										<h4 class="edgtf-pli-share-title">Share</h4>
										<div class="edgtf-social-share-holder edgtf-list">
											<ul>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php echo edgtf_core_get_cpt_shortcode_module_template_part('portfolio', 'portfolio-list', 'pagination/'.$pagination_type, '', $params, $additional_params); ?>
</div>