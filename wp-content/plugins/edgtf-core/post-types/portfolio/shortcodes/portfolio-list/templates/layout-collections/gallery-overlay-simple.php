<?php echo edgtf_core_get_cpt_shortcode_module_template_part('portfolio', 'portfolio-list', 'parts/image', $item_style, $params); ?>

<div class="edgtf-pli-text-holder">
	<div class="edgtf-pli-text-wrapper">
		<div class="edgtf-pli-text">
			<?php echo edgtf_core_get_cpt_shortcode_module_template_part('portfolio', 'portfolio-list', 'parts/title', $item_style, $params); ?>
			
			<?php echo edgtf_core_get_cpt_shortcode_module_template_part('portfolio', 'portfolio-list', 'parts/icon', $item_style, $params); ?>
		</div>
	</div>
</div>