<li class="edgtf-bl-item edgtf-item-space clearfix">
        <div class="edgtf-bli-inner">
		<?php if ( $post_info_image == 'yes' ) {
			$image_size          = isset( $image_size ) ? $image_size : 'full';
			$image_meta          = get_post_meta( get_the_ID(), 'edgtf_blog_list_featured_image_meta', true );
			$has_featured        = ! empty( $image_meta ) || has_post_thumbnail();
			$blog_list_image_id  = ! empty( $image_meta ) && orkan_edge_blog_item_has_link() ? orkan_edge_get_attachment_id_from_url( $image_meta ) : ''; 
		} ?>

		              <div class="vc-hoverbox-wrapper  dual-flipbox vc-hoverbox-shape--square vc-hoverbox-align--center vc-hoverbox-direction--default vc-hoverbox-width--100">
                                <div class="vc-hoverbox" style="perspective: 3576px;">
                                  <div class="vc-hoverbox-inner" style="height: 350px !important;">
                                    <div class="vc-hoverbox-block vc-hoverbox-front" style="background-size:cover;background-position:center;background-image: url(<?php the_post_thumbnail_url($image_size); ?>);">
				      <div class="vc-hoverbox-block-inner vc-hoverbox-front-inner">
                                      </div>
                                    </div>
                                    <div class="vc-hoverbox-block vc-hoverbox-back" data-link="<?php echo get_the_permalink(); ?>">
        			      <div class="vc-hoverbox-block-inner vc-hoverbox-back-inner">
					<?php orkan_edge_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
					<?php if (get_field("movie_author")) { ?><h5>
						<?php if (pll_current_language() == 'de') { ?>von <?php } else if (pll_current_language() == 'en') { ?>by <?php } else { ?>de <?php } ?>
					<?php echo get_field("movie_author"); ?></h5><?php } ?>
					<h5><em><?php echo get_field("movie_context"); ?></em></h5>
					<div class="vc_btn3-container vc_btn3-inline">
					  <a itemprop="url" href="<?php echo get_the_permalink(); ?>" target="_self" class="edgtf-btn edgtf-btn-small edgtf-btn-solid edgtf-st-button">
                                            <span class="edgtf-btn-text"><?php echo pll__("Read more"); ?></span>
                                            <span class="edgtf-btn-hover-item"></span>
                                          </a>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
	</div>
</li>
