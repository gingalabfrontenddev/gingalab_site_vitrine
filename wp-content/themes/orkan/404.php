<?php get_header(); ?>
				<div class="edgtf-page-not-found">
					<?php
					$edgtf_title_image_404 = orkan_edge_options()->getOptionValue( '404_page_title_image' );
					$edgtf_title_404       = orkan_edge_options()->getOptionValue( '404_title' );
					$edgtf_subtitle_404    = orkan_edge_options()->getOptionValue( '404_subtitle' );
					$edgtf_text_404        = orkan_edge_options()->getOptionValue( '404_text' );
					$edgtf_button_label    = orkan_edge_options()->getOptionValue( '404_back_to_home' );
					$edgtf_button_style    = orkan_edge_options()->getOptionValue( '404_button_style' );
					
					if ( ! empty( $edgtf_title_image_404 ) ) { ?>
						<div class="edgtf-404-title-image">
							<img src="<?php echo esc_url( $edgtf_title_image_404 ); ?>" alt="<?php esc_html_e( '404 Title Image', 'orkan' ); ?>" />
						</div>
					<?php } ?>
					
					<h1 class="edgtf-404-title">
						<?php if ( ! empty( $edgtf_title_404 ) ) {
							echo esc_html( $edgtf_title_404 );
						} else {
							esc_html_e( '404', 'orkan' );
						} ?>
					</h1>
					
					<h1 class="edgtf-404-subtitle">
						<?php if ( ! empty( $edgtf_subtitle_404 ) ) {
							echo esc_html( $edgtf_subtitle_404 );
						} else {
							esc_html_e( 'Page can not found', 'orkan' );
						} ?>
					</h1>
					
					<p class="edgtf-404-text">
						<?php if ( ! empty( $edgtf_text_404 ) ) {
							echo esc_html( $edgtf_text_404 );
						} else {
							echo sprintf( esc_html__('The page requested couldn\'t be found. This could be a %s spelling error in the URL or a removed page.', 'orkan' ), '<br />' );
						} ?>
					</p>
					
					<?php
						$button_params = array(
							'size'              => 'large',
                            'type'              => 'solid',
                            'skin'              => 'blue-white',
							'link'              => esc_url( home_url( '/' ) ),
							'text'              => ! empty( $edgtf_button_label ) ? $edgtf_button_label : esc_html__( 'Go back', 'orkan' ),
                            'hover_animation'   => 'yes',
						);
					
						if ( $edgtf_button_style == 'light-style' ) {
							$button_params['custom_class'] = 'edgtf-btn-light-style';
						} else {
							$button_params['custom_class'] = 'edgtf-btn-default-style';
						}
						
						echo orkan_edge_return_button_html( $button_params );
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>