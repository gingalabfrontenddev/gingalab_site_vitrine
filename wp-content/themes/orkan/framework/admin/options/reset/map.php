<?php

if ( ! function_exists( 'orkan_edge_reset_options_map' ) ) {
	/**
	 * Reset options panel
	 */
	function orkan_edge_reset_options_map() {
		
		orkan_edge_add_admin_page(
			array(
				'slug'  => '_reset_page',
				'title' => esc_html__( 'Reset', 'orkan' ),
				'icon'  => 'fa fa-retweet'
			)
		);
		
		$panel_reset = orkan_edge_add_admin_panel(
			array(
				'page'  => '_reset_page',
				'name'  => 'panel_reset',
				'title' => esc_html__( 'Reset', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'reset_to_defaults',
				'default_value' => 'no',
				'label'         => esc_html__( 'Reset to Defaults', 'orkan' ),
				'description'   => esc_html__( 'This option will reset all Select Options values to defaults', 'orkan' ),
				'parent'        => $panel_reset
			)
		);
	}
	
	add_action( 'orkan_edge_options_map', 'orkan_edge_reset_options_map', 100 );
}