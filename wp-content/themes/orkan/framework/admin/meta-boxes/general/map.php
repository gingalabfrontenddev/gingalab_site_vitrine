<?php

if ( ! function_exists( 'orkan_edge_map_general_meta' ) ) {
	function orkan_edge_map_general_meta() {
		
		$general_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => apply_filters( 'orkan_edge_set_scope_for_meta_boxes', array( 'page', 'post' ), 'general_meta' ),
				'title' => esc_html__( 'General', 'orkan' ),
				'name'  => 'general_meta'
			)
		);
		
		/***************** Slider Layout - begin **********************/
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_page_slider_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Slider Shortcode', 'orkan' ),
				'description' => esc_html__( 'Paste your slider shortcode here', 'orkan' ),
				'parent'      => $general_meta_box
			)
		);
		
		/***************** Slider Layout - begin **********************/
		
		/***************** Content Layout - begin **********************/
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_page_content_behind_header_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Always put content behind header', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will put page content behind page header', 'orkan' ),
				'parent'        => $general_meta_box
			)
		);
		
		$edgtf_content_padding_group = orkan_edge_add_admin_group(
			array(
				'name'        => 'content_padding_group',
				'title'       => esc_html__( 'Content Style', 'orkan' ),
				'description' => esc_html__( 'Define styles for Content area', 'orkan' ),
				'parent'      => $general_meta_box
			)
		);
		
			$edgtf_content_padding_row = orkan_edge_add_admin_row(
				array(
					'name'   => 'edgtf_content_padding_row',
					'next'   => true,
					'parent' => $edgtf_content_padding_group
				)
			);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'   => 'edgtf_page_content_top_padding',
						'type'   => 'textsimple',
						'label'  => esc_html__( 'Content Top Padding', 'orkan' ),
						'parent' => $edgtf_content_padding_row,
						'args'   => array(
							'suffix' => 'px'
						)
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'    => 'edgtf_page_content_top_padding_mobile',
						'type'    => 'selectsimple',
						'label'   => esc_html__( 'Set this top padding for mobile header', 'orkan' ),
						'parent'  => $edgtf_content_padding_row,
						'options' => orkan_edge_get_yes_no_select_array( false )
					)
				);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_page_background_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Page Background Color', 'orkan' ),
				'description' => esc_html__( 'Choose background color for page content', 'orkan' ),
				'parent'      => $general_meta_box
			)
		);
		
		/***************** Content Layout - end **********************/
		
		/***************** Boxed Layout - begin **********************/
		
		orkan_edge_add_meta_box_field(
			array(
				'name'    => 'edgtf_boxed_meta',
				'type'    => 'select',
				'label'   => esc_html__( 'Boxed Layout', 'orkan' ),
				'parent'  => $general_meta_box,
				'options' => orkan_edge_get_yes_no_select_array()
			)
		);
		
			$boxed_container_meta = orkan_edge_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'boxed_container_meta',
					'dependency' => array(
						'hide' => array(
							'edgtf_boxed_meta'  => array('','no')
						)
					)
				)
			);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_page_background_color_in_box_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Page Background Color', 'orkan' ),
						'description' => esc_html__( 'Choose the page background color outside box', 'orkan' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_boxed_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'orkan' ),
						'description' => esc_html__( 'Choose an image to be displayed in background', 'orkan' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_boxed_pattern_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Pattern', 'orkan' ),
						'description' => esc_html__( 'Choose an image to be used as background pattern', 'orkan' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'          => 'edgtf_boxed_background_image_attachment_meta',
						'type'          => 'select',
						'default_value' => 'fixed',
						'label'         => esc_html__( 'Background Image Attachment', 'orkan' ),
						'description'   => esc_html__( 'Choose background image attachment', 'orkan' ),
						'parent'        => $boxed_container_meta,
						'options'       => array(
							''       => esc_html__( 'Default', 'orkan' ),
							'fixed'  => esc_html__( 'Fixed', 'orkan' ),
							'scroll' => esc_html__( 'Scroll', 'orkan' )
						)
					)
				);
		
		/***************** Boxed Layout - end **********************/
		
		/***************** Passepartout Layout - begin **********************/
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_paspartu_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Passepartout', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will display passepartout around site content', 'orkan' ),
				'parent'        => $general_meta_box,
				'options'       => orkan_edge_get_yes_no_select_array(),
			)
		);
		
			$paspartu_container_meta = orkan_edge_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'edgtf_paspartu_container_meta',
					'dependency' => array(
						'hide' => array(
							'edgtf_paspartu_meta'  => array('','no')
						)
					)
				)
			);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_paspartu_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Passepartout Color', 'orkan' ),
						'description' => esc_html__( 'Choose passepartout color, default value is #ffffff', 'orkan' ),
						'parent'      => $paspartu_container_meta
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_paspartu_width_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Passepartout Size', 'orkan' ),
						'description' => esc_html__( 'Enter size amount for passepartout', 'orkan' ),
						'parent'      => $paspartu_container_meta,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_paspartu_responsive_width_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Responsive Passepartout Size', 'orkan' ),
						'description' => esc_html__( 'Enter size amount for passepartout for smaller screens (tablets and mobiles view)', 'orkan' ),
						'parent'      => $paspartu_container_meta,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'parent'        => $paspartu_container_meta,
						'type'          => 'select',
						'default_value' => '',
						'name'          => 'edgtf_disable_top_paspartu_meta',
						'label'         => esc_html__( 'Disable Top Passepartout', 'orkan' ),
						'options'       => orkan_edge_get_yes_no_select_array(),
					)
				);
		
		/***************** Passepartout Layout - end **********************/
		
		/***************** Content Width Layout - begin **********************/
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_initial_content_width_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Initial Width of Content', 'orkan' ),
				'description'   => esc_html__( 'Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid")', 'orkan' ),
				'parent'        => $general_meta_box,
				'options'       => array(
					''                => esc_html__( 'Default', 'orkan' ),
					'edgtf-grid-1100' => esc_html__( '1100px', 'orkan' ),
					'edgtf-grid-1300' => esc_html__( '1300px', 'orkan' ),
					'edgtf-grid-1200' => esc_html__( '1200px', 'orkan' ),
					'edgtf-grid-1000' => esc_html__( '1000px', 'orkan' ),
					'edgtf-grid-800'  => esc_html__( '800px', 'orkan' )
				)
			)
		);
		
		/***************** Content Width Layout - end **********************/
		
		/***************** Smooth Page Transitions Layout - begin **********************/
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_smooth_page_transitions_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Smooth Page Transitions', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will perform a smooth transition between pages when clicking on links', 'orkan' ),
				'parent'        => $general_meta_box,
				'options'       => orkan_edge_get_yes_no_select_array()
			)
		);
		
			$page_transitions_container_meta = orkan_edge_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'page_transitions_container_meta',
					'dependency' => array(
						'hide' => array(
							'edgtf_smooth_page_transitions_meta'  => array('','no')
						)
					)
				)
			);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_page_transition_preloader_meta',
						'type'        => 'select',
						'label'       => esc_html__( 'Enable Preloading Animation', 'orkan' ),
						'description' => esc_html__( 'Enabling this option will display an animated preloader while the page content is loading', 'orkan' ),
						'parent'      => $page_transitions_container_meta,
						'options'     => orkan_edge_get_yes_no_select_array()
					)
				);
				
				$page_transition_preloader_container_meta = orkan_edge_add_admin_container(
					array(
						'parent'          => $page_transitions_container_meta,
						'name'            => 'page_transition_preloader_container_meta',
						'dependency' => array(
							'hide' => array(
								'edgtf_page_transition_preloader_meta'  => array('','no')
							)
						)
					)
				);
				
					orkan_edge_add_meta_box_field(
						array(
							'name'   => 'edgtf_smooth_pt_bgnd_color_meta',
							'type'   => 'color',
							'label'  => esc_html__( 'Page Loader Background Color', 'orkan' ),
							'parent' => $page_transition_preloader_container_meta
						)
					);
					
					$group_pt_spinner_animation_meta = orkan_edge_add_admin_group(
						array(
							'name'        => 'group_pt_spinner_animation_meta',
							'title'       => esc_html__( 'Loader Style', 'orkan' ),
							'description' => esc_html__( 'Define styles for loader spinner animation', 'orkan' ),
							'parent'      => $page_transition_preloader_container_meta
						)
					);
					
					$row_pt_spinner_animation_meta = orkan_edge_add_admin_row(
						array(
							'name'   => 'row_pt_spinner_animation_meta',
							'parent' => $group_pt_spinner_animation_meta
						)
					);
					
					orkan_edge_add_meta_box_field(
						array(
							'type'    => 'selectsimple',
							'name'    => 'edgtf_smooth_pt_spinner_type_meta',
							'label'   => esc_html__( 'Spinner Type', 'orkan' ),
							'parent'  => $row_pt_spinner_animation_meta,
							'options' => array(
								''                      => esc_html__( 'Default', 'orkan' ),
								'rotate_circles'        => esc_html__( 'Rotate Circles', 'orkan' ),
								'pulse'                 => esc_html__( 'Pulse', 'orkan' ),
								'double_pulse'          => esc_html__( 'Double Pulse', 'orkan' ),
								'cube'                  => esc_html__( 'Cube', 'orkan' ),
								'rotating_cubes'        => esc_html__( 'Rotating Cubes', 'orkan' ),
								'stripes'               => esc_html__( 'Stripes', 'orkan' ),
								'wave'                  => esc_html__( 'Wave', 'orkan' ),
								'two_rotating_circles'  => esc_html__( '2 Rotating Circles', 'orkan' ),
								'five_rotating_circles' => esc_html__( '5 Rotating Circles', 'orkan' ),
								'atom'                  => esc_html__( 'Atom', 'orkan' ),
								'clock'                 => esc_html__( 'Clock', 'orkan' ),
								'mitosis'               => esc_html__( 'Mitosis', 'orkan' ),
								'lines'                 => esc_html__( 'Lines', 'orkan' ),
								'fussion'               => esc_html__( 'Fussion', 'orkan' ),
								'wave_circles'          => esc_html__( 'Wave Circles', 'orkan' ),
								'pulse_circles'         => esc_html__( 'Pulse Circles', 'orkan' )
							)
						)
					);
					
					orkan_edge_add_meta_box_field(
						array(
							'type'   => 'colorsimple',
							'name'   => 'edgtf_smooth_pt_spinner_color_meta',
							'label'  => esc_html__( 'Spinner Color', 'orkan' ),
							'parent' => $row_pt_spinner_animation_meta
						)
					);
					
					orkan_edge_add_meta_box_field(
						array(
							'name'        => 'edgtf_page_transition_fadeout_meta',
							'type'        => 'select',
							'label'       => esc_html__( 'Enable Fade Out Animation', 'orkan' ),
							'description' => esc_html__( 'Enabling this option will turn on fade out animation when leaving page', 'orkan' ),
							'options'     => orkan_edge_get_yes_no_select_array(),
							'parent'      => $page_transitions_container_meta
						
						)
					);
		
		/***************** Smooth Page Transitions Layout - end **********************/
		
		/***************** Comments Layout - begin **********************/
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_page_comments_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Show Comments', 'orkan' ),
				'description' => esc_html__( 'Enabling this option will show comments on your page', 'orkan' ),
				'parent'      => $general_meta_box,
				'options'     => orkan_edge_get_yes_no_select_array()
			)
		);
		
		/***************** Comments Layout - end **********************/
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_map_general_meta', 10 );
}