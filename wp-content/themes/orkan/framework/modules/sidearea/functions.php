<?php
if ( ! function_exists( 'orkan_edge_register_side_area_sidebar' ) ) {
	/**
	 * Register side area sidebar
	 */
	function orkan_edge_register_side_area_sidebar() {
		register_sidebar(
			array(
				'id'            => 'sidearea',
				'name'          => esc_html__( 'Side Area', 'orkan' ),
				'description'   => esc_html__( 'Side Area', 'orkan' ),
				'before_widget' => '<div id="%1$s" class="widget edgtf-sidearea %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="edgtf-widget-title">',
				'after_title'   => '</h5>'
			)
		);
	}
	
	add_action( 'widgets_init', 'orkan_edge_register_side_area_sidebar' );
}

if ( ! function_exists( 'orkan_edge_side_menu_body_class' ) ) {
	/**
	 * Function that adds body classes for different side menu styles
	 *
	 * @param $classes array original array of body classes
	 *
	 * @return array modified array of classes
	 */
	function orkan_edge_side_menu_body_class( $classes ) {
		
		if ( is_active_widget( false, false, 'edgtf_side_area_opener' ) ) {
			
			$classes[] = 'edgtf-side-menu-slide-from-right';
		}
		
		return $classes;
	}
	
	add_filter( 'body_class', 'orkan_edge_side_menu_body_class' );
}

if ( ! function_exists( 'orkan_edge_get_side_area' ) ) {
	/**
	 * Loads side area HTML
	 */
	function orkan_edge_get_side_area() {
		
		if ( is_active_widget( false, false, 'edgtf_side_area_opener' ) ) {
			
			orkan_edge_get_module_template_part( 'templates/sidearea', 'sidearea' );
		}
	}
	
	add_action( 'orkan_edge_after_body_tag', 'orkan_edge_get_side_area', 10 );
}