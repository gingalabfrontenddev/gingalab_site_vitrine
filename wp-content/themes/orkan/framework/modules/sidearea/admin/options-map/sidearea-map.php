<?php

if ( ! function_exists( 'orkan_edge_sidearea_options_map' ) ) {
	function orkan_edge_sidearea_options_map() {
		
		orkan_edge_add_admin_page(
			array(
				'slug'  => '_side_area_page',
				'title' => esc_html__( 'Side Area', 'orkan' ),
				'icon'  => 'fa fa-indent'
			)
		);
		
		$side_area_panel = orkan_edge_add_admin_panel(
			array(
				'title' => esc_html__( 'Side Area', 'orkan' ),
				'name'  => 'side_area',
				'page'  => '_side_area_page'
			)
		);
		
		$side_area_icon_style_group = orkan_edge_add_admin_group(
			array(
				'parent'      => $side_area_panel,
				'name'        => 'side_area_icon_style_group',
				'title'       => esc_html__( 'Side Area Icon Style', 'orkan' ),
				'description' => esc_html__( 'Define styles for Side Area icon', 'orkan' )
			)
		);
		
		$side_area_icon_style_row1 = orkan_edge_add_admin_row(
			array(
				'parent' => $side_area_icon_style_group,
				'name'   => 'side_area_icon_style_row1'
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row1,
				'type'   => 'colorsimple',
				'name'   => 'side_area_icon_color',
				'label'  => esc_html__( 'Color', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row1,
				'type'   => 'colorsimple',
				'name'   => 'side_area_icon_hover_color',
				'label'  => esc_html__( 'Hover Color', 'orkan' )
			)
		);
		
		$side_area_icon_style_row2 = orkan_edge_add_admin_row(
			array(
				'parent' => $side_area_icon_style_group,
				'name'   => 'side_area_icon_style_row2',
				'next'   => true
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row2,
				'type'   => 'colorsimple',
				'name'   => 'side_area_close_icon_color',
				'label'  => esc_html__( 'Close Icon Color', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row2,
				'type'   => 'colorsimple',
				'name'   => 'side_area_close_icon_hover_color',
				'label'  => esc_html__( 'Close Icon Hover Color', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $side_area_panel,
				'type'          => 'text',
				'name'          => 'side_area_width',
				'default_value' => '',
				'label'         => esc_html__( 'Side Area Width', 'orkan' ),
				'description'   => esc_html__( 'Enter a width for Side Area', 'orkan' ),
				'args'          => array(
					'col_width' => 3,
					'suffix'    => 'px'
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'      => $side_area_panel,
				'type'        => 'color',
				'name'        => 'side_area_background_color',
				'label'       => esc_html__( 'Background Color', 'orkan' ),
				'description' => esc_html__( 'Choose a background color for Side Area', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'      => $side_area_panel,
				'type'        => 'text',
				'name'        => 'side_area_padding',
				'label'       => esc_html__( 'Padding', 'orkan' ),
				'description' => esc_html__( 'Define padding for Side Area in format top right bottom left', 'orkan' ),
				'args'        => array(
					'col_width' => 3
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $side_area_panel,
				'type'          => 'selectblank',
				'name'          => 'side_area_aligment',
				'default_value' => '',
				'label'         => esc_html__( 'Text Alignment', 'orkan' ),
				'description'   => esc_html__( 'Choose text alignment for side area', 'orkan' ),
				'options'       => array(
					''       => esc_html__( 'Default', 'orkan' ),
					'left'   => esc_html__( 'Left', 'orkan' ),
					'center' => esc_html__( 'Center', 'orkan' ),
					'right'  => esc_html__( 'Right', 'orkan' )
				)
			)
		);
	}
	
	add_action( 'orkan_edge_options_map', 'orkan_edge_sidearea_options_map', 15 );
}