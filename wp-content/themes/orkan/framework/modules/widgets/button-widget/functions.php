<?php

if ( ! function_exists( 'orkan_edge_register_button_widget' ) ) {
	/**
	 * Function that register button widget
	 */
	function orkan_edge_register_button_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassButtonWidget';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_button_widget' );
}