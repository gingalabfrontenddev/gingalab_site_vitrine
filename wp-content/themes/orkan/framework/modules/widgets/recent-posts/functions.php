<?php

if ( ! function_exists( 'orkan_edge_register_recent_posts_widget' ) ) {
	/**
	 * Function that register search opener widget
	 */
	function orkan_edge_register_recent_posts_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassRecentPosts';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_recent_posts_widget' );
}