<?php

if ( ! function_exists( 'orkan_edge_register_sidearea_opener_widget' ) ) {
	/**
	 * Function that register sidearea opener widget
	 */
	function orkan_edge_register_sidearea_opener_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassSideAreaOpener';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_sidearea_opener_widget' );
}