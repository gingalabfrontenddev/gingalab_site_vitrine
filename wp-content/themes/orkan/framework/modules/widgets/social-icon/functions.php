<?php

if ( ! function_exists( 'orkan_edge_register_social_icon_widget' ) ) {
	/**
	 * Function that register social icon widget
	 */
	function orkan_edge_register_social_icon_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassSocialIconWidget';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_social_icon_widget' );
}