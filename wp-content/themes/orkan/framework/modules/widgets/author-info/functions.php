<?php

if ( ! function_exists( 'orkan_edge_register_author_info_widget' ) ) {
	/**
	 * Function that register author info widget
	 */
	function orkan_edge_register_author_info_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassAuthorInfoWidget';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_author_info_widget' );
}