<?php

if ( ! function_exists( 'orkan_edge_register_custom_font_widget' ) ) {
	/**
	 * Function that register custom font widget
	 */
	function orkan_edge_register_custom_font_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassCustomFontWidget';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_custom_font_widget' );
}