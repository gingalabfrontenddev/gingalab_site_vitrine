<?php

if ( ! function_exists( 'orkan_edge_register_search_opener_widget' ) ) {
	/**
	 * Function that register search opener widget
	 */
	function orkan_edge_register_search_opener_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassSearchOpener';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_search_opener_widget' );
}