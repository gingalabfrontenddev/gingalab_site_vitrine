<?php

if(!function_exists('orkan_edge_register_sticky_sidebar_widget')) {
	/**
	 * Function that register sticky sidebar widget
	 */
	function orkan_edge_register_sticky_sidebar_widget($widgets) {
		$widgets[] = 'OrkanEdgeClassStickySidebar';
		
		return $widgets;
	}
	
	add_filter('orkan_edge_register_widgets', 'orkan_edge_register_sticky_sidebar_widget');
}