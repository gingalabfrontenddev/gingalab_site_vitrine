<?php

if ( ! function_exists( 'orkan_edge_register_icon_widget' ) ) {
	/**
	 * Function that register icon widget
	 */
	function orkan_edge_register_icon_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassIconWidget';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_icon_widget' );
}