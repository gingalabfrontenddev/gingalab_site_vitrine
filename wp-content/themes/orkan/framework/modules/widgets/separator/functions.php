<?php

if ( ! function_exists( 'orkan_edge_register_separator_widget' ) ) {
	/**
	 * Function that register separator widget
	 */
	function orkan_edge_register_separator_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassSeparatorWidget';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_separator_widget' );
}