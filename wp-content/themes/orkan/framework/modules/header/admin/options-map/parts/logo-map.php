<?php

if ( ! function_exists( 'orkan_edge_logo_options_map' ) ) {
	function orkan_edge_logo_options_map() {
		
		orkan_edge_add_admin_page(
			array(
				'slug'  => '_logo_page',
				'title' => esc_html__( 'Logo', 'orkan' ),
				'icon'  => 'fa fa-coffee'
			)
		);
		
		$panel_logo = orkan_edge_add_admin_panel(
			array(
				'page'  => '_logo_page',
				'name'  => 'panel_logo',
				'title' => esc_html__( 'Logo', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $panel_logo,
				'type'          => 'yesno',
				'name'          => 'hide_logo',
				'default_value' => 'no',
				'label'         => esc_html__( 'Hide Logo', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will hide logo image', 'orkan' )
			)
		);
		
		$hide_logo_container = orkan_edge_add_admin_container(
			array(
				'parent'          => $panel_logo,
				'name'            => 'hide_logo_container',
				'dependency' => array(
					'hide' => array(
						'hide_logo'  => 'yes'
					)
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'logo_image',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT . "/img/logo.png",
				'label'         => esc_html__( 'Logo Image - Default', 'orkan' ),
				'parent'        => $hide_logo_container
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'logo_image_dark',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT . "/img/logo.png",
				'label'         => esc_html__( 'Logo Image - Dark', 'orkan' ),
				'parent'        => $hide_logo_container
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'logo_image_light',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT . "/img/logo_white.png",
				'label'         => esc_html__( 'Logo Image - Light', 'orkan' ),
				'parent'        => $hide_logo_container
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'logo_image_sticky',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT . "/img/logo_white.png",
				'label'         => esc_html__( 'Logo Image - Sticky', 'orkan' ),
				'parent'        => $hide_logo_container
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'logo_image_mobile',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT . "/img/logo.png",
				'label'         => esc_html__( 'Logo Image - Mobile', 'orkan' ),
				'parent'        => $hide_logo_container
			)
		);
	}
	
	add_action( 'orkan_edge_options_map', 'orkan_edge_logo_options_map', 2 );
}