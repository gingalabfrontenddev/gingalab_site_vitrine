<?php

if ( ! function_exists( 'orkan_edge_disable_behaviors_for_header_vertical' ) ) {
	/**
	 * This function is used to disable sticky header functions that perform processing variables their used in js for this header type
	 */
	function orkan_edge_disable_behaviors_for_header_vertical( $allow_behavior ) {
		return false;
	}
	
	if ( orkan_edge_check_is_header_type_enabled( 'header-vertical', orkan_edge_get_page_id() ) ) {
		add_filter( 'orkan_edge_allow_sticky_header_behavior', 'orkan_edge_disable_behaviors_for_header_vertical' );
		add_filter( 'orkan_edge_allow_content_boxed_layout', 'orkan_edge_disable_behaviors_for_header_vertical' );
	}
}