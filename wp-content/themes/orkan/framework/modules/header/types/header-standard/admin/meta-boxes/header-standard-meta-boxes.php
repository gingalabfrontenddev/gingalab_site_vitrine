<?php

if ( ! function_exists( 'orkan_edge_get_hide_dep_for_header_standard_meta_boxes' ) ) {
	function orkan_edge_get_hide_dep_for_header_standard_meta_boxes() {
		$hide_dep_options = apply_filters( 'orkan_edge_header_standard_hide_meta_boxes', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'orkan_edge_header_standard_meta_map' ) ) {
	function orkan_edge_header_standard_meta_map( $parent ) {
		$hide_dep_options = orkan_edge_get_hide_dep_for_header_standard_meta_boxes();
		
		orkan_edge_add_meta_box_field(
			array(
				'parent'          => $parent,
				'type'            => 'select',
				'name'            => 'edgtf_set_menu_area_position_meta',
				'default_value'   => '',
				'label'           => esc_html__( 'Choose Menu Area Position', 'orkan' ),
				'description'     => esc_html__( 'Select menu area position in your header', 'orkan' ),
				'options'         => array(
					''       => esc_html__( 'Default', 'orkan' ),
					'left'   => esc_html__( 'Left', 'orkan' ),
					'right'  => esc_html__( 'Right', 'orkan' ),
					'center' => esc_html__( 'Center', 'orkan' )
				),
				'dependency' => array(
					'hide' => array(
						'edgtf_header_type_meta'  => $hide_dep_options
					)
				)
			)
		);
	}
	
	add_action( 'orkan_edge_additional_header_area_meta_boxes_map', 'orkan_edge_header_standard_meta_map' );
}