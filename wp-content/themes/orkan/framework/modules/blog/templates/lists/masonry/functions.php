<?php

if ( ! function_exists( 'orkan_edge_register_blog_masonry_template_file' ) ) {
	/**
	 * Function that register blog masonry template
	 */
	function orkan_edge_register_blog_masonry_template_file( $templates ) {
		$templates['blog-masonry'] = esc_html__( 'Blog: Masonry', 'orkan' );
		
		return $templates;
	}
	
	add_filter( 'orkan_edge_register_blog_templates', 'orkan_edge_register_blog_masonry_template_file' );
}

if ( ! function_exists( 'orkan_edge_set_blog_masonry_type_global_option' ) ) {
	/**
	 * Function that set blog list type value for global blog option map
	 */
	function orkan_edge_set_blog_masonry_type_global_option( $options ) {
		$options['masonry'] = esc_html__( 'Blog: Masonry', 'orkan' );
		
		return $options;
	}
	
	add_filter( 'orkan_edge_blog_list_type_global_option', 'orkan_edge_set_blog_masonry_type_global_option' );
}