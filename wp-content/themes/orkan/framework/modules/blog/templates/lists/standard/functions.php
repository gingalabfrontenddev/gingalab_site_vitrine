<?php

if ( ! function_exists( 'orkan_edge_register_blog_standard_template_file' ) ) {
	/**
	 * Function that register blog standard template
	 */
	function orkan_edge_register_blog_standard_template_file( $templates ) {
		$templates['blog-standard'] = esc_html__( 'Blog: Standard', 'orkan' );
		
		return $templates;
	}
	
	add_filter( 'orkan_edge_register_blog_templates', 'orkan_edge_register_blog_standard_template_file' );
}

if ( ! function_exists( 'orkan_edge_set_blog_standard_type_global_option' ) ) {
	/**
	 * Function that set blog list type value for global blog option map
	 */
	function orkan_edge_set_blog_standard_type_global_option( $options ) {
		$options['standard'] = esc_html__( 'Blog: Standard', 'orkan' );
		
		return $options;
	}
	
	add_filter( 'orkan_edge_blog_list_type_global_option', 'orkan_edge_set_blog_standard_type_global_option' );
}