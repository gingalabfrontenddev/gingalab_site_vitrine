<div class="edgtf-post-info-author">
    <a itemprop="author" class="edgtf-post-info-author-link" href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>">
        <span class="edgtf-post-info-author-image">
	        <?php echo orkan_edge_kses_img( get_avatar( get_the_author_meta( 'ID' ), 20 ) ); ?>
        </span>
	    <span class="edgtf-post-info-author-text"><?php esc_html_e('by', 'orkan'); ?></span>
	    <span class="edgtf-post-info-author-label"><?php the_author_meta('display_name'); ?></span>
    </a>
</div>