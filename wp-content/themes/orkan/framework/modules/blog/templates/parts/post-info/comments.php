<?php if(comments_open()) { ?>
	<div class="edgtf-post-info-comments-holder">
		<a itemprop="url" class="edgtf-post-info-comments" href="<?php comments_link(); ?>" target="_self">
			<i class="fa fa-comment"></i>
			<?php comments_number('0 ' . esc_html__('Comments','orkan'), '1 '.esc_html__('Comment','orkan'), '% '.esc_html__('Comments','orkan') ); ?>
		</a>
	</div>
<?php } ?>