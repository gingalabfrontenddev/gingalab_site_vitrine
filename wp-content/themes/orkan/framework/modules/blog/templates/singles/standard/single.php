<?php

orkan_edge_get_single_post_format_html($blog_single_type);

do_action('orkan_edge_after_article_content');

orkan_edge_get_module_template_part('templates/parts/single/author-info', 'blog');

orkan_edge_get_module_template_part('templates/parts/single/single-navigation', 'blog');

orkan_edge_get_module_template_part('templates/parts/single/related-posts', 'blog', '', $single_info_params);

orkan_edge_get_module_template_part('templates/parts/single/comments', 'blog');