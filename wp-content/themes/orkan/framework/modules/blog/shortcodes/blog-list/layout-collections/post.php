<li class="edgtf-bl-item edgtf-item-space clearfix">
	<div class="edgtf-bli-inner">
		<?php if ( $post_info_image == 'yes' ) {
			orkan_edge_get_module_template_part( 'templates/parts/media', 'blog', '', $params );
		} ?>
		<div class="edgtf-bli-content">
			<?php if ( $post_info_date == 'yes' || $post_info_category == 'yes' ) { ?>
				<div class="edgtf-bli-info edgtf-bli-info-top">
					<?php
					if ( $post_info_date == 'yes' ) {
						orkan_edge_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params );
					}
					if ( $post_info_category == 'yes' ) {
						orkan_edge_get_module_template_part( 'templates/parts/post-info/category', 'blog', '', $params );
					}
					?>
				</div>
			<?php } ?>
			
			<?php orkan_edge_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
			
			<div class="edgtf-bli-excerpt">
				<?php orkan_edge_get_module_template_part( 'templates/parts/excerpt', 'blog', '', $params ); ?>
			</div>
			
			<?php if ( $post_info_author == 'yes' ) { ?>
				<div class="edgtf-bli-info edgtf-bli-info-bottom">
					<?php orkan_edge_get_module_template_part( 'templates/parts/post-info/author', 'blog', '', $params ); ?>
				</div>
			<?php } ?>
		</div>
	</div>
</li>