<li class="edgtf-blog-slider-item">
	<div class="edgtf-blog-slider-item-inner">
		<div class="edgtf-item-image">
			<a itemprop="url" href="<?php echo get_permalink(); ?>">
				<?php echo get_the_post_thumbnail(get_the_ID(), $image_size); ?>
			</a>
		</div>
		<div class="edgtf-bli-content">
			<div class="edgtf-bli-info edgtf-bli-info-top">
				<?php orkan_edge_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params ); ?>
			</div>
			<?php orkan_edge_get_module_template_part('templates/parts/title', 'blog', '', $params); ?>
		</div>
	</div>
</li>