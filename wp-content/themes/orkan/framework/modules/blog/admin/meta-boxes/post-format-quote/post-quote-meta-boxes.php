<?php

if ( ! function_exists( 'orkan_edge_map_post_quote_meta' ) ) {
	function orkan_edge_map_post_quote_meta() {
		$quote_post_format_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Quote Post Format', 'orkan' ),
				'name'  => 'post_format_quote_meta'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_quote_text_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Quote Text', 'orkan' ),
				'description' => esc_html__( 'Enter Quote text', 'orkan' ),
				'parent'      => $quote_post_format_meta_box
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_quote_author_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Quote Author', 'orkan' ),
				'description' => esc_html__( 'Enter Quote author', 'orkan' ),
				'parent'      => $quote_post_format_meta_box
			)
		);
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_map_post_quote_meta', 25 );
}