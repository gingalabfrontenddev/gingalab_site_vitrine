<?php

if ( ! function_exists( 'orkan_edge_map_post_link_meta' ) ) {
	function orkan_edge_map_post_link_meta() {
		$link_post_format_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Link Post Format', 'orkan' ),
				'name'  => 'post_format_link_meta'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_link_link_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Link', 'orkan' ),
				'description' => esc_html__( 'Enter link', 'orkan' ),
				'parent'      => $link_post_format_meta_box
			)
		);
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_map_post_link_meta', 24 );
}