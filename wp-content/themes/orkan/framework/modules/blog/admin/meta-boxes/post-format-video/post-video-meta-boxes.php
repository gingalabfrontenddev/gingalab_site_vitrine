<?php

if ( ! function_exists( 'orkan_edge_map_post_video_meta' ) ) {
	function orkan_edge_map_post_video_meta() {
		$video_post_format_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Video Post Format', 'orkan' ),
				'name'  => 'post_format_video_meta'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_video_type_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Video Type', 'orkan' ),
				'description'   => esc_html__( 'Choose video type', 'orkan' ),
				'parent'        => $video_post_format_meta_box,
				'default_value' => 'social_networks',
				'options'       => array(
					'social_networks' => esc_html__( 'Video Service', 'orkan' ),
					'self'            => esc_html__( 'Self Hosted', 'orkan' )
				)
			)
		);
		
		$edgtf_video_embedded_container = orkan_edge_add_admin_container(
			array(
				'parent' => $video_post_format_meta_box,
				'name'   => 'edgtf_video_embedded_container'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_video_link_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Video URL', 'orkan' ),
				'description' => esc_html__( 'Enter Video URL', 'orkan' ),
				'parent'      => $edgtf_video_embedded_container,
				'dependency' => array(
					'show' => array(
						'edgtf_video_type_meta' => 'social_networks'
					)
				)
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_video_custom_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Video MP4', 'orkan' ),
				'description' => esc_html__( 'Enter video URL for MP4 format', 'orkan' ),
				'parent'      => $edgtf_video_embedded_container,
				'dependency' => array(
					'show' => array(
						'edgtf_video_type_meta' => 'self'
					)
				)
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_video_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Video Image', 'orkan' ),
				'description' => esc_html__( 'Enter video image', 'orkan' ),
				'parent'      => $edgtf_video_embedded_container,
				'dependency' => array(
					'show' => array(
						'edgtf_video_type_meta' => 'self'
					)
				)
			)
		);
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_map_post_video_meta', 22 );
}