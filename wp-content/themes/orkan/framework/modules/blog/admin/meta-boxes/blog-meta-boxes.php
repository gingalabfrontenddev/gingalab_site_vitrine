<?php

foreach ( glob( EDGE_FRAMEWORK_MODULES_ROOT_DIR . '/blog/admin/meta-boxes/*/*.php' ) as $meta_box_load ) {
	include_once $meta_box_load;
}

if ( ! function_exists( 'orkan_edge_map_blog_meta' ) ) {
	function orkan_edge_map_blog_meta() {
		$edgt_blog_categories = array();
		$categories           = get_categories();
		foreach ( $categories as $category ) {
			$edgt_blog_categories[ $category->slug ] = $category->name;
		}
		
		$blog_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => array( 'page' ),
				'title' => esc_html__( 'Blog', 'orkan' ),
				'name'  => 'blog_meta'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_blog_category_meta',
				'type'        => 'selectblank',
				'label'       => esc_html__( 'Blog Category', 'orkan' ),
				'description' => esc_html__( 'Choose category of posts to display (leave empty to display all categories)', 'orkan' ),
				'parent'      => $blog_meta_box,
				'options'     => $edgt_blog_categories
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_show_posts_per_page_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Number of Posts', 'orkan' ),
				'description' => esc_html__( 'Enter the number of posts to display', 'orkan' ),
				'parent'      => $blog_meta_box,
				'options'     => $edgt_blog_categories,
				'args'        => array(
					'col_width' => 3
				)
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_blog_masonry_layout_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Masonry - Layout', 'orkan' ),
				'description' => esc_html__( 'Set masonry layout. Default is in grid.', 'orkan' ),
				'parent'      => $blog_meta_box,
				'options'     => array(
					''           => esc_html__( 'Default', 'orkan' ),
					'in-grid'    => esc_html__( 'In Grid', 'orkan' ),
					'full-width' => esc_html__( 'Full Width', 'orkan' )
				)
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_blog_masonry_number_of_columns_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Masonry - Number of Columns', 'orkan' ),
				'description' => esc_html__( 'Set number of columns for your masonry blog lists', 'orkan' ),
				'parent'      => $blog_meta_box,
				'options'     => array(
					''      => esc_html__( 'Default', 'orkan' ),
					'two'   => esc_html__( '2 Columns', 'orkan' ),
					'three' => esc_html__( '3 Columns', 'orkan' ),
					'four'  => esc_html__( '4 Columns', 'orkan' ),
					'five'  => esc_html__( '5 Columns', 'orkan' ),
					'six'   => esc_html__( '6 Columns', 'orkan' )
				)
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_blog_masonry_space_between_items_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Masonry - Space Between Items', 'orkan' ),
				'description' => esc_html__( 'Set space size between posts for your masonry blog lists', 'orkan' ),
				'options'     => orkan_edge_get_space_between_items_array( true ),
				'parent'      => $blog_meta_box
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_blog_list_featured_image_proportion_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Masonry - Featured Image Proportion', 'orkan' ),
				'description'   => esc_html__( 'Choose type of proportions you want to use for featured images on masonry blog lists', 'orkan' ),
				'parent'        => $blog_meta_box,
				'default_value' => '',
				'options'       => array(
					''         => esc_html__( 'Default', 'orkan' ),
					'fixed'    => esc_html__( 'Fixed', 'orkan' ),
					'original' => esc_html__( 'Original', 'orkan' )
				)
			)
		);

		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_blog_list_article_appear_fx_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Masonry - Article Appear Effect', 'orkan' ),
				'default_value' => '',
				'description'   => esc_html__( 'Choose whether to have article appear animation when list is loaded.', 'orkan' ),
				'parent'        => $blog_meta_box,
				'options'       => array(
					''    	=> esc_html__( 'Default', 'orkan' ),
					'no'    => esc_html__( 'No', 'orkan' ),
					'yes' 	=> esc_html__( 'Yes', 'orkan' )
				)
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_blog_pagination_type_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Pagination Type', 'orkan' ),
				'description'   => esc_html__( 'Choose a pagination layout for Blog Lists', 'orkan' ),
				'parent'        => $blog_meta_box,
				'default_value' => '',
				'options'       => array(
					''                => esc_html__( 'Default', 'orkan' ),
					'standard'        => esc_html__( 'Standard', 'orkan' ),
					'load-more'       => esc_html__( 'Load More', 'orkan' ),
					'infinite-scroll' => esc_html__( 'Infinite Scroll', 'orkan' ),
					'no-pagination'   => esc_html__( 'No Pagination', 'orkan' )
				)
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'type'          => 'text',
				'name'          => 'edgtf_number_of_chars_meta',
				'default_value' => '',
				'label'         => esc_html__( 'Number of Words in Excerpt', 'orkan' ),
				'description'   => esc_html__( 'Enter a number of words in excerpt (article summary). Default value is 40', 'orkan' ),
				'parent'        => $blog_meta_box,
				'args'          => array(
					'col_width' => 3
				)
			)
		);
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_map_blog_meta', 30 );
}