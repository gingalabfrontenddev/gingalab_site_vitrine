<?php

if ( ! function_exists( 'orkan_edge_map_footer_meta' ) ) {
	function orkan_edge_map_footer_meta() {
		
		$footer_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => apply_filters( 'orkan_edge_set_scope_for_meta_boxes', array( 'page', 'post' ), 'footer_meta' ),
				'title' => esc_html__( 'Footer', 'orkan' ),
				'name'  => 'footer_meta'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_disable_footer_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Disable Footer for this Page', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will hide footer on this page', 'orkan' ),
				'options'       => orkan_edge_get_yes_no_select_array(),
				'parent'        => $footer_meta_box
			)
		);
		
		$show_footer_meta_container = orkan_edge_add_admin_container(
			array(
				'name'       => 'edgtf_show_footer_meta_container',
				'parent'     => $footer_meta_box,
				'dependency' => array(
					'hide' => array(
						'edgtf_disable_footer_meta' => 'yes'
					)
				)
			)
		);
		
			orkan_edge_add_meta_box_field(
				array(
					'name'          => 'edgtf_show_footer_top_meta',
					'type'          => 'select',
					'default_value' => '',
					'label'         => esc_html__( 'Show Footer Top', 'orkan' ),
					'description'   => esc_html__( 'Enabling this option will show Footer Top area', 'orkan' ),
					'options'       => orkan_edge_get_yes_no_select_array(),
					'parent'        => $show_footer_meta_container
				)
			);
			
			orkan_edge_add_meta_box_field(
				array(
					'name'          => 'edgtf_show_footer_bottom_meta',
					'type'          => 'select',
					'default_value' => '',
					'label'         => esc_html__( 'Show Footer Bottom', 'orkan' ),
					'description'   => esc_html__( 'Enabling this option will show Footer Bottom area', 'orkan' ),
					'options'       => orkan_edge_get_yes_no_select_array(),
					'parent'        => $show_footer_meta_container
				)
			);
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_map_footer_meta', 70 );
}