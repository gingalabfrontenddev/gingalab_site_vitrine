<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="edgtf-search-cover" method="get">
	<?php if ( $search_in_grid ) { ?>
	<div class="edgtf-container">
		<div class="edgtf-container-inner clearfix">
	<?php } ?>
			<div class="edgtf-form-holder-outer">
				<div class="edgtf-form-holder">
					<div class="edgtf-form-holder-inner">
						<input type="text" placeholder="<?php esc_html_e( 'Search', 'orkan' ); ?>" name="s" class="edgtf_search_field" autocomplete="off" />
						<a class="edgtf-search-close" href="#">
							<?php echo wp_kses( $search_icon_close, array(
								'span' => array(
									'aria-hidden' => true,
									'class'       => true
								),
								'i'    => array(
									'class' => true
								)
							) ); ?>
						</a>
					</div>
				</div>
			</div>
	<?php if ( $search_in_grid ) { ?>
		</div>
	</div>
	<?php } ?>
</form>