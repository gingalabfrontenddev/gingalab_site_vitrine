<?php

if ( ! function_exists( 'orkan_edge_search_options_map' ) ) {
	function orkan_edge_search_options_map() {
		
		orkan_edge_add_admin_page(
			array(
				'slug'  => '_search_page',
				'title' => esc_html__( 'Search', 'orkan' ),
				'icon'  => 'fa fa-search'
			)
		);
		
		$search_page_panel = orkan_edge_add_admin_panel(
			array(
				'title' => esc_html__( 'Search Page', 'orkan' ),
				'name'  => 'search_template',
				'page'  => '_search_page'
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'search_page_layout',
				'type'          => 'select',
				'label'         => esc_html__( 'Layout', 'orkan' ),
				'default_value' => 'in-grid',
				'description'   => esc_html__( 'Set layout. Default is in grid.', 'orkan' ),
				'parent'        => $search_page_panel,
				'options'       => array(
					'in-grid'    => esc_html__( 'In Grid', 'orkan' ),
					'full-width' => esc_html__( 'Full Width', 'orkan' )
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'search_page_sidebar_layout',
				'type'          => 'select',
				'label'         => esc_html__( 'Sidebar Layout', 'orkan' ),
				'description'   => esc_html__( "Choose a sidebar layout for search page", 'orkan' ),
				'default_value' => 'no-sidebar',
				'options'       => orkan_edge_get_custom_sidebars_options(),
				'parent'        => $search_page_panel
			)
		);
		
		$orkan_custom_sidebars = orkan_edge_get_custom_sidebars();
		if ( count( $orkan_custom_sidebars ) > 0 ) {
			orkan_edge_add_admin_field(
				array(
					'name'        => 'search_custom_sidebar_area',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Sidebar to Display', 'orkan' ),
					'description' => esc_html__( 'Choose a sidebar to display on search page. Default sidebar is "Sidebar"', 'orkan' ),
					'parent'      => $search_page_panel,
					'options'     => $orkan_custom_sidebars,
					'args'        => array(
						'select2' => true
					)
				)
			);
		}
		
		$search_panel = orkan_edge_add_admin_panel(
			array(
				'title' => esc_html__( 'Search', 'orkan' ),
				'name'  => 'search',
				'page'  => '_search_page'
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $search_panel,
				'type'          => 'select',
				'name'          => 'search_icon_pack',
				'default_value' => 'font_awesome',
				'label'         => esc_html__( 'Search Icon Pack', 'orkan' ),
				'description'   => esc_html__( 'Choose icon pack for search icon', 'orkan' ),
				'options'       => orkan_edge_icon_collections()->getIconCollectionsExclude( array( 'linea_icons' ) )
			)
		);

        orkan_edge_add_admin_field(
            array(
                'type'          => 'select',
                'name'          => 'search_sidebar_columns',
                'parent'        => $search_panel,
                'default_value' => '3',
                'label'         => esc_html__( 'Search Sidebar Columns', 'orkan' ),
                'description'   => esc_html__( 'Choose number of columns for FullScreen search sidebar area', 'orkan' ),
                'options'       => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                )
            )
        );
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $search_panel,
				'type'          => 'yesno',
				'name'          => 'search_in_grid',
				'default_value' => 'no',
				'label'         => esc_html__( 'Enable Grid Layout', 'orkan' ),
				'description'   => esc_html__( 'Set search area to be in grid. (Applied for Search covers header types.', 'orkan' ),
			)
		);
		
		orkan_edge_add_admin_section_title(
			array(
				'parent' => $search_panel,
				'name'   => 'initial_header_icon_title',
				'title'  => esc_html__( 'Initial Search Icon in Header', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $search_panel,
				'type'          => 'text',
				'name'          => 'header_search_icon_size',
				'default_value' => '',
				'label'         => esc_html__( 'Icon Size', 'orkan' ),
				'description'   => esc_html__( 'Set size for icon', 'orkan' ),
				'args'          => array(
					'col_width' => 3,
					'suffix'    => 'px'
				)
			)
		);
		
		$search_icon_color_group = orkan_edge_add_admin_group(
			array(
				'parent'      => $search_panel,
				'title'       => esc_html__( 'Icon Colors', 'orkan' ),
				'description' => esc_html__( 'Define color style for icon', 'orkan' ),
				'name'        => 'search_icon_color_group'
			)
		);
		
		$search_icon_color_row = orkan_edge_add_admin_row(
			array(
				'parent' => $search_icon_color_group,
				'name'   => 'search_icon_color_row'
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent' => $search_icon_color_row,
				'type'   => 'colorsimple',
				'name'   => 'header_search_icon_color',
				'label'  => esc_html__( 'Color', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent' => $search_icon_color_row,
				'type'   => 'colorsimple',
				'name'   => 'header_search_icon_hover_color',
				'label'  => esc_html__( 'Hover Color', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $search_panel,
				'type'          => 'yesno',
				'name'          => 'enable_search_icon_text',
				'default_value' => 'no',
				'label'         => esc_html__( 'Enable Search Icon Text', 'orkan' ),
				'description'   => esc_html__( "Enable this option to show 'Search' text next to search icon in header", 'orkan' )
			)
		);
		
		$enable_search_icon_text_container = orkan_edge_add_admin_container(
			array(
				'parent'          => $search_panel,
				'name'            => 'enable_search_icon_text_container',
				'dependency' => array(
					'show' => array(
						'enable_search_icon_text' => 'yes'
					)
				)
			)
		);
		
		$enable_search_icon_text_group = orkan_edge_add_admin_group(
			array(
				'parent'      => $enable_search_icon_text_container,
				'title'       => esc_html__( 'Search Icon Text', 'orkan' ),
				'name'        => 'enable_search_icon_text_group',
				'description' => esc_html__( 'Define style for search icon text', 'orkan' )
			)
		);
		
		$enable_search_icon_text_row = orkan_edge_add_admin_row(
			array(
				'parent' => $enable_search_icon_text_group,
				'name'   => 'enable_search_icon_text_row'
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent' => $enable_search_icon_text_row,
				'type'   => 'colorsimple',
				'name'   => 'search_icon_text_color',
				'label'  => esc_html__( 'Text Color', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent' => $enable_search_icon_text_row,
				'type'   => 'colorsimple',
				'name'   => 'search_icon_text_color_hover',
				'label'  => esc_html__( 'Text Hover Color', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $enable_search_icon_text_row,
				'type'          => 'textsimple',
				'name'          => 'search_icon_text_font_size',
				'label'         => esc_html__( 'Font Size', 'orkan' ),
				'default_value' => '',
				'args'          => array(
					'suffix' => 'px'
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $enable_search_icon_text_row,
				'type'          => 'textsimple',
				'name'          => 'search_icon_text_line_height',
				'label'         => esc_html__( 'Line Height', 'orkan' ),
				'default_value' => '',
				'args'          => array(
					'suffix' => 'px'
				)
			)
		);
		
		$enable_search_icon_text_row2 = orkan_edge_add_admin_row(
			array(
				'parent' => $enable_search_icon_text_group,
				'name'   => 'enable_search_icon_text_row2',
				'next'   => true
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $enable_search_icon_text_row2,
				'type'          => 'selectblanksimple',
				'name'          => 'search_icon_text_text_transform',
				'label'         => esc_html__( 'Text Transform', 'orkan' ),
				'default_value' => '',
				'options'       => orkan_edge_get_text_transform_array()
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $enable_search_icon_text_row2,
				'type'          => 'fontsimple',
				'name'          => 'search_icon_text_google_fonts',
				'label'         => esc_html__( 'Font Family', 'orkan' ),
				'default_value' => '-1',
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $enable_search_icon_text_row2,
				'type'          => 'selectblanksimple',
				'name'          => 'search_icon_text_font_style',
				'label'         => esc_html__( 'Font Style', 'orkan' ),
				'default_value' => '',
				'options'       => orkan_edge_get_font_style_array(),
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $enable_search_icon_text_row2,
				'type'          => 'selectblanksimple',
				'name'          => 'search_icon_text_font_weight',
				'label'         => esc_html__( 'Font Weight', 'orkan' ),
				'default_value' => '',
				'options'       => orkan_edge_get_font_weight_array(),
			)
		);
		
		$enable_search_icon_text_row3 = orkan_edge_add_admin_row(
			array(
				'parent' => $enable_search_icon_text_group,
				'name'   => 'enable_search_icon_text_row3',
				'next'   => true
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'parent'        => $enable_search_icon_text_row3,
				'type'          => 'textsimple',
				'name'          => 'search_icon_text_letter_spacing',
				'label'         => esc_html__( 'Letter Spacing', 'orkan' ),
				'default_value' => '',
				'args'          => array(
					'suffix' => 'px'
				)
			)
		);
	}
	
	add_action( 'orkan_edge_options_map', 'orkan_edge_search_options_map', 16 );
}