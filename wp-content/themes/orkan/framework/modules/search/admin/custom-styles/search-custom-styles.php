<?php

if ( ! function_exists( 'orkan_edge_search_opener_icon_size' ) ) {
	function orkan_edge_search_opener_icon_size() {
		$icon_size = orkan_edge_options()->getOptionValue( 'header_search_icon_size' );
		
		if ( ! empty( $icon_size ) ) {
			echo orkan_edge_dynamic_css( '.edgtf-search-opener', array(
				'font-size' => orkan_edge_filter_px( $icon_size ) . 'px'
			) );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_search_opener_icon_size' );
}

if ( ! function_exists( 'orkan_edge_search_opener_icon_colors' ) ) {
	function orkan_edge_search_opener_icon_colors() {
		$icon_color       = orkan_edge_options()->getOptionValue( 'header_search_icon_color' );
		$icon_hover_color = orkan_edge_options()->getOptionValue( 'header_search_icon_hover_color' );
		
		if ( ! empty( $icon_color ) ) {
			echo orkan_edge_dynamic_css( '.edgtf-search-opener', array(
				'color' => $icon_color
			) );
		}
		
		if ( ! empty( $icon_hover_color ) ) {
			echo orkan_edge_dynamic_css( '.edgtf-search-opener:hover', array(
				'color' => $icon_hover_color
			) );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_search_opener_icon_colors' );
}

if ( ! function_exists( 'orkan_edge_search_opener_text_styles' ) ) {
	function orkan_edge_search_opener_text_styles() {
		$item_styles = orkan_edge_get_typography_styles( 'search_icon_text' );
		
		$item_selector = array(
			'.edgtf-search-icon-text'
		);
		
		echo orkan_edge_dynamic_css( $item_selector, $item_styles );
		
		$text_hover_color = orkan_edge_options()->getOptionValue( 'search_icon_text_color_hover' );
		
		if ( ! empty( $text_hover_color ) ) {
			echo orkan_edge_dynamic_css( '.edgtf-search-opener:hover .edgtf-search-icon-text', array(
				'color' => $text_hover_color
			) );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_search_opener_text_styles' );
}