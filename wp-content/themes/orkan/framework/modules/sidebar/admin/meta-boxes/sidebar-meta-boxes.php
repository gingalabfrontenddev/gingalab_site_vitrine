<?php

if ( ! function_exists( 'orkan_edge_map_sidebar_meta' ) ) {
	function orkan_edge_map_sidebar_meta() {
		$edgtf_sidebar_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => apply_filters( 'orkan_edge_set_scope_for_meta_boxes', array( 'page' ), 'sidebar_meta' ),
				'title' => esc_html__( 'Sidebar', 'orkan' ),
				'name'  => 'sidebar_meta'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_sidebar_layout_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Sidebar Layout', 'orkan' ),
				'description' => esc_html__( 'Choose the sidebar layout', 'orkan' ),
				'parent'      => $edgtf_sidebar_meta_box,
                'options'       => orkan_edge_get_custom_sidebars_options( true )
			)
		);
		
		$edgtf_custom_sidebars = orkan_edge_get_custom_sidebars();
		if ( count( $edgtf_custom_sidebars ) > 0 ) {
			orkan_edge_add_meta_box_field(
				array(
					'name'        => 'edgtf_custom_sidebar_area_meta',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Choose Widget Area in Sidebar', 'orkan' ),
					'description' => esc_html__( 'Choose Custom Widget area to display in Sidebar"', 'orkan' ),
					'parent'      => $edgtf_sidebar_meta_box,
					'options'     => $edgtf_custom_sidebars,
					'args'        => array(
						'select2' => true
					)
				)
			);
		}
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_map_sidebar_meta', 31 );
}