<?php

if ( ! function_exists( 'orkan_edge_sidebar_options_map' ) ) {
	function orkan_edge_sidebar_options_map() {
		
		orkan_edge_add_admin_page(
			array(
				'slug'  => '_sidebar_page',
				'title' => esc_html__( 'Sidebar Area', 'orkan' ),
				'icon'  => 'fa fa-indent'
			)
		);
		
		$sidebar_panel = orkan_edge_add_admin_panel(
			array(
				'title' => esc_html__( 'Sidebar Area', 'orkan' ),
				'name'  => 'sidebar',
				'page'  => '_sidebar_page'
			)
		);
		
		orkan_edge_add_admin_field( array(
			'name'          => 'sidebar_layout',
			'type'          => 'select',
			'label'         => esc_html__( 'Sidebar Layout', 'orkan' ),
			'description'   => esc_html__( 'Choose a sidebar layout for pages', 'orkan' ),
			'parent'        => $sidebar_panel,
			'default_value' => 'no-sidebar',
            'options'       => orkan_edge_get_custom_sidebars_options()
		) );
		
		$orkan_custom_sidebars = orkan_edge_get_custom_sidebars();
		if ( count( $orkan_custom_sidebars ) > 0 ) {
			orkan_edge_add_admin_field( array(
				'name'        => 'custom_sidebar_area',
				'type'        => 'selectblank',
				'label'       => esc_html__( 'Sidebar to Display', 'orkan' ),
				'description' => esc_html__( 'Choose a sidebar to display on pages. Default sidebar is "Sidebar"', 'orkan' ),
				'parent'      => $sidebar_panel,
				'options'     => $orkan_custom_sidebars,
				'args'        => array(
					'select2' => true
				)
			) );
		}
	}
	
	add_action( 'orkan_edge_options_map', 'orkan_edge_sidebar_options_map', 9 );
}