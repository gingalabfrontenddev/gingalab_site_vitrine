<?php

if ( ! function_exists( 'orkan_edge_get_title_types_meta_boxes' ) ) {
	function orkan_edge_get_title_types_meta_boxes() {
		$title_type_options = apply_filters( 'orkan_edge_title_type_meta_boxes', $title_type_options = array( '' => esc_html__( 'Default', 'orkan' ) ) );
		
		return $title_type_options;
	}
}

foreach ( glob( EDGE_FRAMEWORK_MODULES_ROOT_DIR . '/title/types/*/admin/meta-boxes/*.php' ) as $meta_box_load ) {
	include_once $meta_box_load;
}

if ( ! function_exists( 'orkan_edge_map_title_meta' ) ) {
	function orkan_edge_map_title_meta() {
		$title_type_meta_boxes = orkan_edge_get_title_types_meta_boxes();
		
		$title_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => apply_filters( 'orkan_edge_set_scope_for_meta_boxes', array( 'page', 'post' ), 'title_meta' ),
				'title' => esc_html__( 'Title', 'orkan' ),
				'name'  => 'title_meta'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_show_title_area_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'orkan' ),
				'description'   => esc_html__( 'Disabling this option will turn off page title area', 'orkan' ),
				'parent'        => $title_meta_box,
				'options'       => orkan_edge_get_yes_no_select_array()
			)
		);
		
			$show_title_area_meta_container = orkan_edge_add_admin_container(
				array(
					'parent'          => $title_meta_box,
					'name'            => 'edgtf_show_title_area_meta_container',
					'dependency' => array(
						'hide' => array(
							'edgtf_show_title_area_meta' => 'no'
						)
					)
				)
			);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_type_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Area Type', 'orkan' ),
						'description'   => esc_html__( 'Choose title type', 'orkan' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => $title_type_meta_boxes
					)
				);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_in_grid_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Area In Grid', 'orkan' ),
						'description'   => esc_html__( 'Set title area content to be in grid', 'orkan' ),
						'options'       => orkan_edge_get_yes_no_select_array(),
						'parent'        => $show_title_area_meta_container
					)
				);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_title_area_height_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Height', 'orkan' ),
						'description' => esc_html__( 'Set a height for Title Area', 'orkan' ),
						'parent'      => $show_title_area_meta_container,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px'
						)
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_title_area_background_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Background Color', 'orkan' ),
						'description' => esc_html__( 'Choose a background color for title area', 'orkan' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_title_area_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'orkan' ),
						'description' => esc_html__( 'Choose an Image for title area', 'orkan' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_background_image_behavior_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Background Image Behavior', 'orkan' ),
						'description'   => esc_html__( 'Choose title area background image behavior', 'orkan' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => array(
							''                    => esc_html__( 'Default', 'orkan' ),
							'hide'                => esc_html__( 'Hide Image', 'orkan' ),
							'responsive'          => esc_html__( 'Enable Responsive Image', 'orkan' ),
							'responsive-disabled' => esc_html__( 'Disable Responsive Image', 'orkan' ),
							'parallax'            => esc_html__( 'Enable Parallax Image', 'orkan' ),
							'parallax-zoom-out'   => esc_html__( 'Enable Parallax With Zoom Out Image', 'orkan' ),
							'parallax-disabled'   => esc_html__( 'Disable Parallax Image', 'orkan' )
						)
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_vertical_alignment_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Vertical Alignment', 'orkan' ),
						'description'   => esc_html__( 'Specify title area content vertical alignment', 'orkan' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => array(
							''              => esc_html__( 'Default', 'orkan' ),
							'header-bottom' => esc_html__( 'From Bottom of Header', 'orkan' ),
							'window-top'    => esc_html__( 'From Window Top', 'orkan' )
						)
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_title_tag_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Tag', 'orkan' ),
						'options'       => orkan_edge_get_title_tag( true, array( 'span' => esc_html__( 'Custom Heading', 'orkan' ) ) ),
						'parent'        => $show_title_area_meta_container
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_title_text_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Title Color', 'orkan' ),
						'description' => esc_html__( 'Choose a color for title text', 'orkan' ),
						'parent'      => $show_title_area_meta_container
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_subtitle_meta',
						'type'          => 'text',
						'default_value' => '',
						'label'         => esc_html__( 'Subtitle Text', 'orkan' ),
						'description'   => esc_html__( 'Enter your subtitle text', 'orkan' ),
						'parent'        => $show_title_area_meta_container,
						'args'          => array(
							'col_width' => 6
						)
					)
				);
		
				orkan_edge_add_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_subtitle_tag_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Subtitle Tag', 'orkan' ),
						'options'       => orkan_edge_get_title_tag( true, array( 'p' => 'p' ) ),
						'parent'        => $show_title_area_meta_container
					)
				);
				
				orkan_edge_add_meta_box_field(
					array(
						'name'        => 'edgtf_subtitle_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Subtitle Color', 'orkan' ),
						'description' => esc_html__( 'Choose a color for subtitle text', 'orkan' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
		/***************** Additional Title Area Layout - start *****************/
		
		do_action( 'orkan_edge_additional_title_area_meta_boxes', $show_title_area_meta_container );
		
		/***************** Additional Title Area Layout - end *****************/
		
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_map_title_meta', 60 );
}