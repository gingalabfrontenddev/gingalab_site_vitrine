<?php

if ( ! function_exists( 'orkan_edge_breadcrumbs_title_type_options_meta_boxes' ) ) {
	function orkan_edge_breadcrumbs_title_type_options_meta_boxes( $show_title_area_meta_container ) {
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_breadcrumbs_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Breadcrumbs Color', 'orkan' ),
				'description' => esc_html__( 'Choose a color for breadcrumbs text', 'orkan' ),
				'parent'      => $show_title_area_meta_container
			)
		);
	}
	
	add_action( 'orkan_edge_additional_title_area_meta_boxes', 'orkan_edge_breadcrumbs_title_type_options_meta_boxes' );
}