<?php

if ( ! function_exists( 'orkan_edge_include_woocommerce_shortcodes' ) ) {
	function orkan_edge_include_woocommerce_shortcodes() {
		foreach ( glob( EDGE_FRAMEWORK_MODULES_ROOT_DIR . '/woocommerce/shortcodes/*/load.php' ) as $shortcode_load ) {
			include_once $shortcode_load;
		}
	}
	
	if ( orkan_edge_core_plugin_installed() ) {
		add_action( 'edgtf_core_action_include_shortcodes_file', 'orkan_edge_include_woocommerce_shortcodes' );
	}
}

if ( ! function_exists( 'orkan_edge_set_product_list_icon_class_name_for_vc_shortcodes' ) ) {
	/**
	 * Function that set custom icon class name for product shortcodes to set our icon for Visual Composer shortcodes panel
	 */
	function orkan_edge_set_product_list_icon_class_name_for_vc_shortcodes( $shortcodes_icon_class_array ) {
		$shortcodes_icon_class_array[] = '.icon-wpb-product-list';
		
		return $shortcodes_icon_class_array;
	}
	
	if ( orkan_edge_core_plugin_installed() ) {
		add_filter( 'edgtf_core_filter_add_vc_shortcodes_custom_icon_class', 'orkan_edge_set_product_list_icon_class_name_for_vc_shortcodes' );
	}
}