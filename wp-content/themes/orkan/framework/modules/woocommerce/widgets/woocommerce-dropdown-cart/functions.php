<?php

if ( ! function_exists( 'orkan_edge_register_woocommerce_dropdown_cart_widget' ) ) {
	/**
	 * Function that register image gallery widget
	 */
	function orkan_edge_register_woocommerce_dropdown_cart_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassWoocommerceDropdownCart';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_woocommerce_dropdown_cart_widget' );
}